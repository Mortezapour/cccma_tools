#!/bin/bash
set -e
#~~~~~~~~~~~
# Functions
#~~~~~~~~~~~
SETUP_SCRIPT_NAME=$(basename $0)
function usage(){
    echo "$SETUP_SCRIPT_NAME: [-f] runid=abc123 ver=my-version config=ESM [repo=CANESM_SRC_REPO] [fetch_method=clone|link|copy] [production=0|1]"
    echo "  FLAGS"
    echo "      -f(orce)        : if experiment or source directories are already found for this runid, delete them, unprompted"
    echo ""
    echo "  ARGS"
    echo "      runid           : represents the unique identifier for the desired run - MUST BE UNIQUE. Don't re-use runids"
    echo "                          unless the new run is meant to REPLACE the previous one and was ran with the current account."
    echo ""
    echo "      ver             : defines the commit/branch/tag to use to setup the run."
    echo "                          Note that this argument is ignored if 'fetch_method' is set to copy or link so the"
    echo "                          version of the source repo is respected."
    echo ""
    echo "      config          : defines the high level model configuration - currently accepts one of AMIP/OMIP/ESM or"
    echo "                          REFENV, which is used to build a reference environment from the ESM config"
    echo ""
    echo "      repo            : defines the location of the CanESM git repo - can be a path to an on-disk repo or"
    echo "                          git-url (see fetch_method). Defaults to git@gitlab.science.gc.ca:CanESM/CanESM5.git."
    echo ""
    echo "      fetch_method    : defines how the source repo will be retrieved - currently accepts one of clone/link/copy."
    echo "                          Note that using 'clone' with repo= pointing to an on-disk repo is not currently supported."
    echo "                          Defaults to clone."
    echo ""
    echo "      production      : if '1' activates strict production settings. Defaults to '0'"

}

function parse_arguments(){
    # Define defaults
    canesm_repo="git@gitlab.science.gc.ca:CanESM/CanESM5.git"
    fetch_method="clone"
    production=0
    delete_existing_dirs=0

    # Flags
    while getopts hf opt; do
        case $opt in
            f) delete_existing_dirs=1;;
            h) usage; exit 0 ;;
            ?) echo "Invalid flag!"; usage; exit 1;;
        esac
    done
    shift $(( OPTIND - 1 ))

    # Arguments
    for arg in "$@"; do
        case $arg in
            *=*)
                var=$(echo $arg | awk -F\= '{printf "%s",$1}')
                val=$(echo $arg | awk -F\= '{printf "%s",$2}')
                case $var in 
                    runid)          canesm_runid="$val"     ;;
                    ver)            canesm_ver="$val"       ;;
                    config)         canesm_config="$val"    ;;
                    repo)           canesm_repo="$val"      ;;
                    fetch_method)   fetch_method="$val"     ;;
                    production)     production="$val"       ;;
                    *)              echo "Invalid command line arg ---> $arg <---"; exit 1 ;;
                esac ;;
              *) echo "Invalid command line arg ---> $arg <---"; exit 1 ;;
        esac
    done
    SETUP_ARGS="runid=$canesm_runid ver=$canesm_ver config=$canesm_config repo=$canesm_repo fetch_method=$fetch_method production=$production"

    # Check that we got the required args
    [[ -z $canesm_runid ]]  && { echo "runid is a required argument! See '$SETUP_SCRIPT_NAME -h'"; exit 1;}
    [[ -z $canesm_config ]] && { echo "config is a required argument! See '$SETUP_SCRIPT_NAME -h'"; exit 1;}

    # make sure that the rest of the options make sense/will work
    if ! [[ $fetch_method =~ ^(clone|link|copy)$ ]]; then
        echo "fetch_method must be one of clone, link, or copy! See '$SETUP_SCRIPT_NAME -h'"
        exit 1
    fi

    if ! [[ $production =~ ^(0|1)$ ]]; then
        echo "production must be 0 or 1! See '$SETUP_SCRIPT_NAME -h'"
        exit 1
    fi

    if [[ $fetch_method == "clone" ]]; then
        # we need a version if cloning
        [[ -z $canesm_ver ]] && { echo "ver is a required argument when cloning from the source repo! See '$SETUP_SCRIPT_NAME -h'"; exit 1;}

        # due to problems with recursive cloning of on-disk super repo, we don't currently support this fetch method
        #   if an on-disk repo is provided
        if ! is_git_url $canesm_repo; then
            echo "cloning from an on-disk repo is not currently supported! Either use fetch_method=copy or fetch_method=link"
            exit 1
        fi
    else
        # make sure we didn't get a git url for the repo, and make sure the directory exists if one was given
        if is_git_url $canesm_repo; then
            echo "fetch_method=link|copy requires that repo= points to an ON-DISK repo! Either use fetch_method=clone or"
            echo "  point to an on-disk repo"
            exit 1
        else
            if [[ ! -e $canesm_repo ]]; then
                echo "$canesm_repo doesn't exist!"
                exit 1
            fi
        fi

        # tell user that the version argument will be ignored to respect the version in the source repo
        if [[ ! -z $canesm_ver ]]; then
            echo ""
            echo "for fetch_method=link|copy, the version in the source repo will be used!"
            echo "  Ignoring ver=$canesm_ver.."
            echo ""
        fi
    fi
    return 0
}

function is_git_url(){
    repo_location=$1
    if [[ $repo_location == *".git" ]]; then
        exit_status=0
    else
        exit_status=1
    fi
    return $exit_status
}

function fetch_canesm_repo(){
    temp_canesm_clone_location=tmp_${canesm_runid}_canesm_clone_$$
    if [[ $fetch_method == "clone" ]]; then
        git clone $canesm_repo $temp_canesm_clone_location
        (cd $temp_canesm_clone_location; git checkout $canesm_ver; git submodule update --recursive --init --checkout)
    else
        cp -r ${canesm_repo}/ $temp_canesm_clone_location
    fi
    return 0
}

function version_controlled_setup(){
    source ${temp_canesm_clone_location}/CCCma_tools/tools/adv-setup
    return 0
}

#~~~~~~~~~~~~~~
# Main Program
#~~~~~~~~~~~~~~
parse_arguments $@
if [[ $fetch_method == "link" ]]; then
    temp_canesm_clone_location=$canesm_repo
else
    fetch_canesm_repo
fi
version_controlled_setup

