#! /bin/sh

#    Aug 12/99 - F.Majaess 
#
#id  conv_ccc2rpn - Used to convert CCRN standard binary datafile into RPN format.
#
#    AUTHOR  - F.Majaess.
#
#  PURPOSE - "conv_ccc2rpn" script, given the CCRN standard binary format 
#            filename in the first argument, it produces from it based on the 
#            second argument, an RPN format datafile.
#            Note: Only available on pollux.
#
#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     arg1    = Input; path/filename of CCRN standard binary format datafile.
#pr
#pr     arg2    = Output; path/filename of RPN datafile.
#pr
#pr     arg3    = Switch to control stripping cyclical longitude.
#pr               "nolaslon" is the only valid setting for it.
#pr               (default: is to strip cyclical longitude)
#pr

#ex EXAMPLE:
#ex
#ex   conv_ccc2rpn ccrnfile rpnfile  [nolaslon]
#ex
#

# echo " no. or arfguments = $#"
if [ $# -lt 2 ] ; then
 echo "conv_ccc2rpn: Invalid call: $@ !"
 exit 1
else
 if [ $# -gt 2 ] ; then
   if [ "$3" == "nolaslon" ] ; then
    nolaslon='yes'
   else
    echo "conv_ccc2rpn: Invalid third argument $3 !"
    exit 2
   fi
 fi
fi
if [ ! -s $1 ] ; then
 echo "conv_ccc2rpn: Sorry, input file $1 is non existing or empty!"
 exit 3
fi
if [ -f $2 ] ; then
 echo "conv_ccc2rpn: $2 already exists, please remove and re-invoke"
 exit 4
fi
ARMNLIB=/usr/local/env/armnlib
export ARMNLIB
PATH="${PATH}:${ARMNLIB}:${ARMNLIB}/bin:${ARMNLIB}/scripts"
# Note: TMPDIR must be defined.
TMPDIR=`pwd`
export TMPDIR 
set -x
if [ "$nolaslon" == 'yes' ] ; then
 r.diag convert -rpn $2 -ccrn $1 -keep rpn -info ERRORS -opktyp PK92 
else
 r.diag convert -rpn $2 -ccrn $1 -keep rpn -info ERRORS -laslon -opktyp PK92 
fi &&
exit || exit 5
