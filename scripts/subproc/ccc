#!/bin/bash

#   Oct 28/08 - F. Majaess (Pass captued input directly instead of via
#                           "Input_Cards" file).
#   Jun 24/97 - D. Liu     (rewrite of the script to avoid using 
#                           positional parameter input=...)
#   Feb 18/97 - F. Majaess (Revise to ensure "input=..." added before
#                           "output=...", if any)
#   Dec 16/96 - D. Liu

#id ccc     - script to be used before CCCma diagnostic programs on
#id           the command line to handle redirection of standard
#id           input.

#   AUTHOR  - D. Liu

#hd PURPOSE - 'ccc' handles redirection of standard input for CCCma
#hd           diagnostic programs.
#hd
#hd        Normally, CCCma diagnostic programs read input data from the
#hd        file Input_Cards. This script, used before a diagnostic
#hd        program, reads from the standard input.  If input is provided
#hd        via the standard input, lines starting with '+    .    ' are
#hd        filtered out and the remaining ones are saved in the file 
#hd        .ccc_cards and then prepended to the file Input_Cards. The 
#hd        program called proceeds as usual, namely, reading from the 
#hd        file Input_Cards if it requires an input card.
#hd  
#hd        Unless input redirection is desired and input data is
#hd        provided, this script has no effect on the usual way in which
#hd        a CCCma diagnostic program handles input. It can be either
#hd        left in or taken out from the command line in this case.
#hd  
#hd  NOTES When the standard input is a terminal, 'ccc' will wait
#hd  ===== until something is fed to it via standard input terminated
#hd        usually by the user pressing CTRL-D. While this is convenient
#hd        for using single programs such as 'select' where a user may
#hd        simply type the input card(s) at the terminal followed by
#hd        pressing CTRL-D, it may not be a good idea to run scripts
#hd        this way since input cards are usually contained in the
#hd        scripts and no further input is needed from the terminal. To
#hd        avoid having to wait for input every time 'ccc' is encountered
#hd        in the scripts, run the scripts in the following fashion, 
#hd           sh < myscript & 

#hd
#ex EXAMPLES:
#ex
#ex      1) ccc select modelfile pcp
#ex      2) ccc ggstat pcp
#ex      3) ccc select modelfile st <<EOR
#ex         SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME   ST
#ex         EOR
#ex      4) ccc select modelfile st < mycard
#ex
#ex      Example 1 selects pcp from file modelfile and Example 2
#ex      displays simple statistics on the file pcp. In batch mode, the
#ex      input from standard input for both examples is null. Thus the
#ex      program 'select', which requires input cards, will read from
#ex      the file Input_Cards. Both programs behave as if the script
#ex      'ccc' were not used at all. In fact, the script 'ccc' may be
#ex      left out in this case.
#ex
#ex      Examples 3, 4 select the screen temperature from the file
#ex      modelfile. In the examples, the standard input is handled by the
#ex      script 'ccc' and passed on to the program 'select'. Examples
#ex      3, 4 require that 'ccc' be used.
#ex
#ex      A user may wish to use 'ccc' interactively at a terminal. For
#ex      Example 1 in interactive mode, the user needs to type in the
#ex      input card at a terminal and press CTRL-D to signal the end
#ex      of the input.  If the input is not null, it will be used by
#ex      'select' as input card. Otherwise, the file Input_Cards will
#ex      be used. In Example 2 the program 'ggstat' does not require
#ex      input card, and thus there is no good reason to use 'ccc' in
#ex      front of 'ggstat' in interactive mode although one can
#ex      (pressing CTRL-D is still required). Examples 3 and 4 work in
#ex      the same way as in batch mode.
#ex
#ex      See notes above for running scripts interactively.


set -e                                # activate switch to immediate escape to
                                      # parent shell upon an error condition

(rm -f .ccc_cards || : )

sed -e '/^+   .    /d' > .ccc_cards   # read from standard input, strip off 
                                      # comment lines that start with  
                                      # '+   .    ', and then save the
                                      # remaining lines into file ".ccc_cards"
# set -x
if [ -s .ccc_cards ]; then            # if .ccc_cards is non-zero in size

   FirstArg=`echo $@ | $AWK '{ print $1 ; }'`
   if [ "$FirstArg" = 'xsaveup' ] ; then  # Special handling for "xsaveup"
    if [ -s Input_Cards ]; then           # if Input_Cards not empty
      echo '*EOR' >> .ccc_cards           #    append separator "*EOR" to .ccc_cards
      cat Input_Cards >>.ccc_cards        #    append Input_Cards to .ccc_cards
    fi
    mv .ccc_cards Input_Cards             # rename .ccc_cards as Input_Cards
    exec $@                               # execute command line
   else
                                          # Check if "output=" presence
    ccc_cmd=`echo $@ | sed -e 's/^.*output=.*$/output/'`
    if [ "$ccc_cmd" = 'output' ] ; then
                                          # Ensure "input=" preceeds "output="
     ccc_cmd=`echo $@ | sed -e 's/ output=/ input=.ccc_cards output=/'`
     exec $ccc_cmd                        # execute command line with ".ccc_cards" input added
    else
     exec $@ input=.ccc_cards             # execute command line with ".ccc_cards" input added
    fi
   fi
                                          # file supplied to the program via "input=".
else                                      # otherwise
   (rm -f .ccc_cards || : )
   exec $@                                # execute command line
fi
