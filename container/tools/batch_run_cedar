#!/bin/bash
#SBATCH --time=12:00:0
#SBATCH --constraint=cascade
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=48
#SBATCH --mem-per-cpu=1000
#SBATCH --job-name=run_canesm

set -x
CWD=$PWD
echo "Working in $(pwd)"
echo "on $HOSTNAME"
config_file=$(pwd)/canesm.cfg

# GET CONFIGURATION VARIABLES
[[ ! -f "$config_file" ]] && ( echo "$config_file must exist!" ; exit 1 )
source $config_file

# Check required variables are defined
[[ ! -f "${CONTAINER_IMAGE}" ]]     && ( echo "CONTAINER_IMAGE must be defined and exist" ; exit 1 )
[[ ! -d "${CANESM_SRC_ROOT}" ]]     && ( echo "Required code CANESM_SRC_ROOT does not exist" ; exit 1 )
[[ ! -d "${EXEC_STORAGE_DIR}" ]]    && ( echo "EXEC_STORAGE_DIR must be defined and must exist" ; exit 1 )
[[ ! -d "${INPUT_DIR}" ]]           && ( echo "INPUT_DIR must be defined and must exist" ; exit 1 )
[[ -z "${OUTPUT_DIR}" ]]            && ( echo "OUTPUT_DIR must be defined" ; exit 1 )
[[ -z "${WRK_DIR}" ]]               && ( echo "WRK_DIR must be defined"; exit 1 )
config_dir="${WRK_DIR}/config"

# Get useful CanESM shell functions
source $CANESM_SRC_ROOT/CCCma_tools/tools/CanESM_shell_functions.sh

# Move to tmpdir
cd $SLURM_TMPDIR

# GET required files and data
srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 cp -rp ${INPUT_DIR}/* $SLURM_TMPDIR
srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 cp -rfp ${EXEC_STORAGE_DIR}/* $SLURM_TMPDIR
srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 cp -fp ${config_dir}/* $SLURM_TMPDIR
srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 cp -fp ${config_dir}/namelists/* $SLURM_TMPDIR

# Get new restarts if we are not in the first year
if [ "$CURRENT_YEAR" -gt "$START_YEAR" ]; then
    last_year=$(( CURRENT_YEAR -1 )) 
    # Collect all required restarts in a directory for a single copy out to nodes
    rs_dir=${OUTPUT_DIR}/${last_year}/restarts_${CURRENT_YEAR}
    mkdir -p ${rs_dir}
    rm -f $rs_dir/*

    # cleanup old restarts
    srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 rm -f ${SLURM_TMPDIR}/restart*.nc

   cp -rp ${OUTPUT_DIR}/${last_year}/cpl_restart_out.nc ${rs_dir}/cpl_restart.nc
   cp -rp ${OUTPUT_DIR}/${last_year}/NEWRS ${rs_dir}/OLDRS
   cp -rp ${OUTPUT_DIR}/${last_year}/NEWTS ${rs_dir}/OLDTS
   
   # Ocean restart files
   cn_ocerst_in='restart'
   cn_ocerst_out='restart'
   rm -f ${cn_ocerst_in}*
   found_rs=`(ls -1 ${OUTPUT_DIR}/${last_year}/*_${cn_ocerst_out}_[0-9][0-9][0-9][0-9].nc || : ) 2>/dev/null`
   for rsfile in $found_rs; do
       sfx=`echo $rsfile|sed 's/^.*\(_[0-9][0-9][0-9][0-9].nc\).*$/\1/'`
       #[ "x$sfx" = "x$rsfile" ] && sfx=`echo $rsfile|sed 's/^.*\(\.nc\).*$/\1/'`
       cp -rp $rsfile ${rs_dir}/${cn_ocerst_in}$sfx
   done

   # Ice restart files
   cn_icerst_out='restart_ice'
   cn_icerst_in='restart_ice_in'
   rm -f ${cn_icerst_in}*
   found_rs=`(ls -1 ${OUTPUT_DIR}/${last_year}/*_${cn_icerst_out}_[0-9][0-9][0-9][0-9].nc || : ) 2>/dev/null`
   for rsfile in $found_rs; do
       sfx=`echo $rsfile|sed 's/^.*\(_[0-9][0-9][0-9][0-9].nc\).*$/\1/'`
       cp -rp $rsfile ${rs_dir}/${cn_icerst_in}$sfx
   done

   # trc restart files
   cn_trcrst_out='restart_trc'
   cn_trcrst_in='restart_trc_in'
   rm -f ${cn_trcrst_in}*
   found_rs=`(ls -1 ${OUTPUT_DIR}/${last_year}/*_${cn_trcrst_out}_[0-9][0-9][0-9][0-9].nc || : ) 2>/dev/null`
   for rsfile in $found_rs; do
       sfx=`echo $rsfile|sed 's/^.*\(_[0-9][0-9][0-9][0-9].nc\).*$/\1/'`
       cp -rp $rsfile ${rs_dir}/${cn_trcrst_in}$sfx
   done
   srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 cp -rfp ${rs_dir}/* ${SLURM_TMPDIR}
fi

cd $SLURM_TMPDIR
# Set the execution environment
source runtime_environment

#Get mpich, unload mpi - TODO: Add these too a machine specific platform consideration file for compute canada
#set +x
module --force purge
module load StdEnv/2016.4 nixpkgs/16.09 gcc/7.3.0 mpich/3.2.1 netcdf-fortran/4.4.4
module unload intel openmpi
module load singularity

#ls > run_start_files

# update counters
update_agcm_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 agcm_timestep=900 namelist_file=modl.dat
update_coupler_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 runid=$RUNID namelist_file=nl_coupler_par
if [[ $CONFIG == "ESM" ]]; then
    update_nemo_counters start_date=${CURRENT_YEAR}-01-01 stop_date=${CURRENT_YEAR}-12-31 nemo_timestep=3600 namelist_file=namelist
fi

# Define run command
#   - When setting the run command, the '-n' MPI allocations must match those used in the AGCM compilation and NEMO namelist
container_name=$(basename ${CONTAINER_IMAGE})
if [[ $CONFIG == "ESM" ]]; then
    run_command="mpirun -n 32 singularity exec -B /localscratch ${container_name} ./${agcm_exec} : -n 1 singularity exec -B /localscratch ${container_name} ./${coupler_exec} : -n 56 singularity exec -B /localscratch ${container_name} ./${nemo_exec}"
elif [[ $CONFIG == "AMIP" ]]; then
    run_command="mpirun -n 32 singularity exec -B /localscratch ${container_name} ./${agcm_exec} : -n 1 singularity exec -B /localscratch ${container_name} ./${coupler_exec}"
else
    echo "Unsupported run config"
    exit 1
fi
time ${run_command}

export OUTPUT_DIR=${OUTPUT_DIR}/${CURRENT_YEAR}
mkdir -p ${OUTPUT_DIR}
srun --ntasks=$SLURM_NNODES --ntasks-per-node=1 $CANESM_SRC_ROOT/CCCma_tools/container/tools/nodecp.sh

# check things completed as expected
time_step=$(cat time.step)
nemo_end_step=$(grep -P '(?<!\!)nn_itend[[:space:]]*=.*' namelist | sed 's/nn_itend\s*=\s*//')
if (( nemo_end_step == time_step )) ; then

    # Do diagnostics for current year
    cd $CWD
    sed -i "s/DIAG_YEAR=.*/DIAG_YEAR=${CURRENT_YEAR}/" canesm.cfg
    sbatch --account=$CC_ACCOUNT $CANESM_SRC_ROOT/CCCma_tools/container/tools/batch_diag_cedar
    
    # Increment CURRENT_YEAR. If <= $STOP_YEAR, resubmit
    next_year=$(( $CURRENT_YEAR + 1 ))
    
    if (( next_year <= STOP_YEAR )) ; then
        # update current year
        sed -i "s/CURRENT_YEAR=.*/CURRENT_YEAR=${next_year}/" canesm.cfg

        # launch next year
        sbatch --account=$CC_ACCOUNT $CANESM_SRC_ROOT/CCCma_tools/container/tools/batch_run_cedar
    fi
else
    echo "IT LOOKS LIKE THE MODEL CRASHED AND DIDN'T PROPERLY COMPLETE! Check the output files in $OUTPUT_DIR"
fi


