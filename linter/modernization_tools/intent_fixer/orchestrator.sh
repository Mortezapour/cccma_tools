#!/bin/bash

mkdir -p backups_intent
for file in "$@"; do
    filename=${file##*/}
    if [[ -f backups_intent/$filename ]]; then
        echo "Already changed intent for $filename. Skipping..."
        continue
    fi
    echo "Adding vars to $filename..."
    cp $file .
    python3 intent_in.py $filename || exit 1
    gfortran $filename > errors.txt 2>&1 # compiler specified here
    python3 get_inout_vars.py errors.txt > vars.txt || exit 1
    python3 intent_inout.py $filename || exit 1
    cp $file backups_intent/$filename
    yes | mv $filename $file
    rm -f vars.txt
    rm -f errors.txt
done
