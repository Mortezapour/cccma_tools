'''
This tool is used to decapitalize reserved words. The list is not
necessarily exhaustive, and can be added to / subtracted from at will.
The two gotchas in this process are compiler directives (which are case
sensitive), and string literals which are ignored when possible.
'''

import re
import sys

class Decapitalizer(object):
    def __init__(self):
        print('Decapitalizer initialized')
    
    def process(self, lines):
        # insert the joined self.words expression into `rx` and compile it
        self.lines = lines
        self.correctedLines = []

        self.ignoreLines = 0    # flag for the !ignoreLint() directive
        self.directive = 0  # flag for preprocessor directives
        for line in self.lines:
            if self.skippable(line): # check if there's a reason we should be skipping this line
                self.correctedLines.append(line)
            else:
                self.correctedLines.append(self.decapitalize(line))

        return self.correctedLines

    # main decapitalization algorithm
    def decapitalize(self, line):
        # Don't decapitalize Doxygen comments
        rx = r'(^.*?)(?=(!!|!<|!>|\n))(.*)$'
        parsedLine = re.match(rx, line)
        g1 = parsedLine[1]
        g2 = parsedLine[3]
        if '"' in g1 and '\'' in g1:
            print("ERROR: erroneous whitespace parameters")
            pass
        elif '"' in g1:
            g1 = g1.split('"')
            g1 = [self.customLower(x) if i%2 == 0 else x for i, x in enumerate(g1) ]
            g1 = '"'.join(g1)
        elif '\'' in g1:
            # tricky situation; apostraphes in comments should be separated out
            precomment = g1.split('!')
            g1 = precomment[0]
            g1 = g1.split('\'')
            g1 = [self.customLower(x) if i%2 == 0 else x for i, x in enumerate(g1) ]
            g1 = '\''.join(g1)
            if len(precomment) > 1:
                g1 = [g1]
                g1.extend(precomment[1:])
                g1 = '!'.join(g1)
        else:
            g1 = self.customLower(g1)
        
        return g1 + g2 + '\n'
            
    def customLower(self, inputString):
        parsedString = inputString.split(' ')
        lowerWords = [x.lower() if '_PAR_' not in x else x for x in parsedString]
        lowerString = ' '.join(lowerWords)
        return lowerString

    # checks if a line should be skipped by the linter
    def skippable(self, line):
        lineSkip = re.match(r'^.*!(ignoreLint|ignoreLintDecapitalizer)\((\d+)\)', line, re.IGNORECASE) # check for !ignoreLint()
        if lineSkip:
            self.ignoreLines = int(lineSkip.group(2))
            return True
        elif self.ignoreLines > 0:
            self.ignoreLines -= 1
            return True
        elif re.match(r'^[ \t]*#if', line, re.IGNORECASE): # the start of a preprocessor directive
            self.directive += 1
            return True
        elif re.match(r'^[ \t]*#endif', line, re.IGNORECASE): # last line of the directive
            self.directive -= 1
            return True
        else:
            return False
