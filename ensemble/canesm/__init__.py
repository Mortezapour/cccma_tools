from canesm.canesm_ensemble import CanESMensemble
from canesm.canesm_setup import CanESMsetup
from canesm.job_submitter import CanESMsubmitter
from canesm.canesm_database import CanESMensembleDB
from canesm.exceptions import RemoteError

from ._version import get_versions
__version__ = get_versions()['full-revisionid']
del get_versions
