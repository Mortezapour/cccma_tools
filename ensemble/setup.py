# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import versioneer
from os import path

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='canesm-ensemble',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='batch submission of canesm ensemble runs',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Landon Rieger',
    author_email='landon.rieger@canada.ca',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['fabric', 'click', 'pyyaml', 'bokeh>1.0', 'appdirs', 'pandas', 'invoke', 'filelock'],
    entry_points='''
      [console_scripts]
      setup-ensemble=canesm.scripts.setup_ensemble:setup_ensemble
      monitor-ensemble=canesm.scripts.monitor_ensemble:monitor_ensemble
      submit-ensemble=canesm.scripts.submit_ensemble:submit_ensemble
      extend-ensemble=canesm.scripts.extend_ensemble:extend_ensemble
      '''
)
