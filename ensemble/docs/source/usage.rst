.. _usage:

Quick Start Guide
=================

Installation
------------

Using canesm-ensemble on the Science network
********************************************

canesm-ensemble is available on the science network by activating the
``py3_ensemble-cli_v3`` conda environment, which is part of the CCCma wide
``conda`` install, described `here <https://gitlab.science.gc.ca/CanESM/Python_Envs>`_.
Specifically, to activate this environment run

.. code-block:: bash

   export PATH=/home/ords/crd/ccrn/scrd102/ccc_conda/4.6.14/miniconda2/bin:$PATH # if this conda isn't on your PATH
   source activate py3_ensemble-cli_v3

You are now ready to go. 

Using canesm-ensemble from your laptop
**************************************

From a python 3 environment on your laptop you can install the package from gitlab.

.. code-block:: bash

   pip install git+ssh://git@gitlab.science.gc.ca/CanESM/canesm-ensemble.git

If you're new to conda and python 3 see :ref:`setting_up_p3`.

.. _basic_usage:

Basic Usage
-----------

The ensemble setup is controlled by a user-generated
`yaml <https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html>`_
file. A configuration file will look something like this

.. literalinclude:: ../../canesm/setup_examples/example_config.yaml
   :language: yaml

JSON formatted files are also accepted. More details on how to setup a configuration file
can be found in :ref:`options`. You can also read the :ref:`step-by-step guide<step_by_step>`,
or see some :ref:`example configuration files<example_config_files>`.
Once you have the configuration file (eg. ``config.yaml``) the ensemble members can be generated using the script

.. code-block:: bash

   setup-ensemble config.yaml

More information on the command line interfaces is available :ref:`here<cli>`.
By default the jobs are not submitted after configuration, but can be with the option

.. code-block:: bash

   setup-ensemble config.yaml --submit

.. _monitoring_ensembles:

Submitting Ensembles
--------------------

If submitting the entire ensemble using the flag ``--submit`` is not recommended, then
the job can be submitted in batches using the script

.. code-block:: bash

   submit-ensemble config.yaml -n 5

This will submit the next ``n`` jobs to the remote machine. More information on submitting
jobs is available :ref:`here<submit_the_jobs>`


Monitoring Ensembles
--------------------

After ensembles have been launched the overall status can be monitored using

.. code-block:: bash

   monitor-ensemble config.yaml

This will display some basic information in a web page indicating what jobs are running,
queued, and if they have failed. More information on its use can be found :ref:`here<monitor_ensemble>`.
