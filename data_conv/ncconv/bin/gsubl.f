      PROGRAM GSUBL
C     PROGRAM GSUBL (X,       Y,       Z,       OUTPUT,                 )       C2
C    1        TAPE1=X, TAPE2=Y, TAPE3=Z, TAPE6=OUTPUT)
C     ------------------------------------------------                          C2
C                                                                               C2
C     JUN 29/99 - SLAVA KHARIN                                                  C2
C                                                                               C2
CGMLTL    - SUBTRACTS FIRST FILE BY RECORDS OF SECOND FILE              1  1    C1
C                                                                               C3
CAUTHOR  - SLAVA KHARIN                                                         C3
C                                                                               C3
CPURPOSE - FILE  ARITHMETIC PROGRAM THAT SUBTACTS THE FIRST INPUT               C3
C          FILE (X) BY THE RECORDS OF THE SECOND INPUT FILE (Y)                 C3
C          AND PUTS THE RESULTS ON FILE Z. (Z = X * (RECORDS OF) Y)             C3
C          FILE Y IS REWINDED EVERY TIME EOF IS REACHED.                        C3
C          NOTE - NO TYPE CHECKING IS DONE EXCEPT FOR THE REQUIREMENT           C3
C                 THAT THE TWO FILES HAVE SAME TYPE OF RECORDS.                 C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      X = ANY DATA FILE WITH CCRN LABEL                                        C3
C      Y = ANY DATA FILE WITH CCRN LABEL                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      Z = OUTPUT FILE WHOSE CONTENT IS: X * (RECORDS OF) Y                     C3
C---------------------------------------------------------------------------
C

      COMMON/BLANCK/ A(65341), B(65341)
      LOGICAL OK

      COMMON /ICOM/ IBUF(8),IDAT(130682)
      INTEGER JBUF(8)

      DATA MAXX/130682/
C---------------------------------------------------------------------
      NF = 4
      CALL JCLPNT(NF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3

C     * READ THE NEXT FIELD.

      NR = 0
  100 CALL GETFLD2(1,A,-1,-1,-1,-1,IBUF,MAXX,OK)

          IF (.NOT.OK)                                         THEN
              IF(NR.EQ.0) CALL                     XIT('GSUBL',-1)
              WRITE(6,6010) NR
              CALL PRTLAB(IBUF)
              CALL                                 XIT('GSUBL',0)
          ENDIF
          DO I = 1, 8
            JBUF(I)=IBUF(I)
          ENDDO
          KIND = IBUF(1)
          ILG  = IBUF(5)
          ILAT = IBUF(6)
          NWDS = ILG*ILAT
          IF (IBUF(1).EQ.4HSPEC .OR. IBUF(1).EQ.4HFOUR) NWDS = NWDS*2
          IF (NR.EQ.0) CALL PRTLAB(IBUF)

C     * READ THE FIRST RECORD OF Y.

 110      CALL GETFLD2(2,B,KIND,-1,-1,-1,IBUF,MAXX,OK)

          IF (.NOT.OK) THEN
             IF (NR .EQ. 0) CALL                   XIT('GSUBL',-2)
             REWIND 2
             GO TO 110
          ENDIF
          IF (NR.EQ.0) CALL PRTLAB(IBUF)

C         * MAKE SURE THAT A AND B ARE THE SAME SIZE.

          IF (IBUF(5).NE.ILG .OR. IBUF(6).NE.ILAT)             THEN
              CALL PRTLAB(IBUF)
              CALL                                 XIT('GSUBL',-3)
          ENDIF

C         * SUBTRACT THE FIELDS.

          DO 200 I=1,NWDS
              A(I) = A(I)-B(I)
  200     CONTINUE

C         * SAVE THE RESULT ON FILE C.

          DO I = 1, 8
            IBUF(I)=JBUF(I)
          ENDDO
          CALL PUTFLD2(3,A,IBUF,MAXX)

          NR = NR+1
      GOTO 100

C---------------------------------------------------------------------
 6010 FORMAT('0',I6,' RECORDS PROCESSED.')
      END
