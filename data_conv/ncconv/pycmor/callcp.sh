#!/bin/bash
# This is a simple shell script which calls the pycmor interface with some basic options.
# For more options and info see python pycmor.py -h
#
# NCS 12/2018

# make sure path_export.sh has been sourced
bail(){
echo " *** ERROR *** $1"
exit 1
}
[ -z "$NCCONV_DIR" ] && bail 'variable NCCONV_DIR not set.. be sure to source path_export.sh'
[ -z "$HALLN" ] && bail 'variable HALLN not set.. be sure to source path_export.sh'
which pycmor.py > /dev/null 2>&1 || bail 'pycmor.py not on your $PATH variable... source path_export.sh and try again'

# define conversion vars
runid='v5.0-pi-glo-spread-01'
user='scrd103'
chunk='585201_585212'
outpath='.'                                 # output location of netcdf files produced by CMOR
nemo_mesh_mask='nemo_mesh_mask_rc3.nc'      # define the mask file used (only necessary for runs without a 'time-series-like' mask file saved)

project='CMIP6'
version='myversion'

# Set up additional environment considerations
export DATAPATH_DB=/space/hall${HALLN}/sitestore/eccc/crd/ccrn/users/$user/$runid/datapath_local.db:/space/hall${HALLN}/sitestore/eccc/crd/ppp_data/database_dir/datapath_ppp${HALLN}.db

cmor_table='Amon'
pycmor.py -L -r $runid -m $nemo_mesh_mask -t $cmor_table -p $project -k $chunk -o $outpath -v $version -c 'tas pr psl uas vas'

cmor_table='Omon'
# pycmor.py -L -r $runid -m $nemo_mesh_mask -t $cmor_table -p $project -k $chunk -o $outpath -v $version -c 'tos ph zostoga'

cmor_table='SImon'
# pycmor.py -L -r $runid -m $nemo_mesh_mask -t $cmor_table -p $project -k $chunk -o $outpath -v $version -c 'siconc'


