#!/usr/bin/python
'''
Compare content in CMIP6 data request (dreqPy software, aka "dreq") to content in CMIP6 json tables (https://github.com/PCMDI/cmip6-cmor-tables).

This is a stand-alone script that can be run to verify consistency between the dreq content and the json tables that we use in pycmor. 
Versions of the json tables indicate a dreq version, e.g. in https://github.com/PCMDI/cmip6-cmor-tables/blob/master/Tables/CMIP6_3hr.json:
    "data_specs_version": "01.00.29"
As long as we use the corresponding versions of the dreq and json tables, the info should be the same. This script checks if they are.

JA, 22 Jan 2019
'''

import os
import sys
import json

# Compare json tables content to dreq content
compare_to_dreq = True

# Compare json tables content to google spreadsheet content (25jan.19: not implemented. Probably won't.)
compare_to_gs = not True


show_dreq_grids_info = not True




test1 = True # testing on James's laptop


dreq_version = '01.00.29'
'''
IMPORTANT: if changing the dreq version, a new python instance needs to be started. 
If running this as part of pycmor, this is no problem because a new python instance is always started when running it.
If running in ipython, need to restart ipython to ensure reloading of a different dreq version.
Once the module is loaded it seems not possible to completely unload it. reload(module) or imp.reload(module) don't do it.
From looking around stackexchange I get impression python doesn't support fully unloading modules.
This is not really important for this script and for pycmor since we don't expect to be changing the dreq version often, 
it's mainly just for testing and to see impact of a given change in dreq version.
'''

ncconv_dir = '/home/rja001/netcdf_conversion/ncconv' # could define from config-pycmor
dreq_path = os.path.join(ncconv_dir, 'cmor_tools/CMIP6dreq', 'dreqPy')

if test1: dreq_path = os.path.join('/HOME/rja/code/cmip6', dreq_version, 'dreqPy')


###############################################################################
# Functions

# Default location and name for logfile (but the calling script could change it if desired)
logfile_dir = '.'
if not os.path.isdir(logfile_dir): os.makedirs(logfile_dir) 
logfile = 'compare_dreq_to_json.log'    # default name for logfile
logfile = os.path.join(logfile_dir, logfile) # full path to logfile

def msg(w, excep=False):
    write_logfile = not True
    show_msgs = True
    w = str(w)
    if excep: w += '\nAborting'
    if show_msgs:
        print w
    if write_logfile: 
        # Append to logfile.
        # If we don't want to see the results of previous calls, the calling 
        # program can remove the logfile.
        with open(logfile, 'a') as f:
            f.write(w + '\n')
    if excep:
        # replace this later with proper exception handling (10jan.19)
        assert False

def chksme(x):
    '''
    Return unique entries of input list (i.e. filter out any duplicates).
    '''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]

def checkUnique(secName='', attName='', dq=None, return_nonunique_items=False):
    '''
    Check to see if an attribute has 1-to-1 mapping to unique items in a section.
    E.g. I like to use 'label' to identify items because it's more intuitive, makes the code easier to understand and debug.
    For attName='label' in a given section, e.g. secName='experiment', this function will return true if label uniquely identifies items.
    Can also make attName a list of attributes, and then the function checks to see if this combination of attributes gives a 1-to-1 mapping.
    '''
    l = dq.coll[secName].items
    l_uid = [itm.uid for itm in l]
    if         isinstance(attName, (str,unicode)):
        # Get the attribute value for each item in the list.
        l_att = [getattr(itm,attName) for itm in l]
    elif    isinstance(attName, (list,tuple)):
        '''
        If attName gives >1 attribute name, we check to see if this combo of attributes has a 1-to-1 mapping.
        (Rather than just a single attribute having a 1-to-1 mapping.)
        So we make a tuple of the attributes, and then proceed the same way as if just using a single attribute.
        E.g.
            secName = 'CMORvar'
            attName = ('mipTable', 'label')
        Then (showing just the first 5 list entries) we'll get
            lt = [['Amon', 'Omon', 'Amn', 'Lmon', 'CCMI1_monthly'], ['ch4', 'vsfsit', 'clivi', 'nep', 'clc']]
        which zip(*lt) then transforms into
            l_att = [('Amon', 'ch4'), ('Omon', 'vsfsit'), ('Amon', 'clivi'), ('Lmon', 'nep'), ('CCMI1_monthly', 'clc')]
        '''
        l_attName = attName
        lt = []
        for attName in l_attName:
            lt += [ [getattr(itm,attName) for itm in l] ]
        l_att = zip(*lt)
        del lt
        attName = l_attName
    l_uid2 = chksme(l_uid)
    l_att2 = chksme(l_att)
    if not True:
        print 'in dreqTools.checkUnique:', secName, attName
        print len(l_uid2), len(l_uid)
    assert len(l_uid2) == len(l), 'There should be a unique uid for every item in the section'
    assert len(l_uid2) == len(l_uid), 'Why is uid not unique?'
    assert len(l_att) == len(l_uid), 'Both lists are from same section - how did this happen?'
    if len(l_att2) != len(l_att):
        print 'attribute = ' + str(attName) + ' is not a unique identifier in section = ' + secName
        print len(l_att), len(l_att2)
        if return_nonunique_items:
            # Display the items that are not uniquely identified and return them.
            ind = filter(lambda m: l_att[m] in l_att[:m], range(len(l_att)))
            return [l[m] for m in ind]
        else: 
            return False
    else:
        return True    

def find_attribute(attName):
    '''
    For a given attribute name (e.g. "comment", "description") find out which sections have items with this attribute.
    '''
    l = []
    for secName in dq.coll.keys():
        li = dq.coll[secName].items
        if len(li) == 0: continue
        itm = li[0]
        if hasattr(itm, attName):
            l += [secName]
    return l
    
def find_attValue(v, l_sec=[], l_att=[]):
    '''
    For a given attribute value, e.g. some string or a number, search through items in the data request that have that value. 
    Search is done over list l_sec of sections and l_att of attribute names. If these lists are empty then all sections and/or
    attributes are searched over.
    '''    
    if len(l_sec) == 0: l_sec = dq.coll.keys()
    li = []
    v = str(v)
    for secName in l_sec:
        l_sec_att = dq.coll[secName].attDefn.keys()
        if len(l_att) == 0: l_att_search = l_sec_att
        for att in l_att_search:
            if att not in l_sec_att: continue
            li1 = [itm for itm in dq.coll[secName].items if v in str(getattr(itm,att))]    
            li1 = [itm for itm in li1 if itm not in li] # in case any duplicates
            li += li1
    return li

def getItemsByAttr(secName='', attName='', l=[], assertUnique=False):
    '''
    Get list containing the items in a section corresponding to a list of attribute values.
    E.g. l = list of itm.label values, then a list of items (itm) corresponding to these values are returned.
    Here itm denotes an item in the section, i.e. an element from the dq.coll[secName].items list.
    For this to work, the association between the attribute (e.g. label) and need not be unique, but set unique=True if we want it to be.
    '''
    if assertUnique: assert checkUnique(secName,attName,dq), 'Not a unique association between attribute = ' + attName + ' and items in section = ' + secName
    l_att = l
    l_itm = []
    assert secName in dq.coll
    preserve_order = True    # True ==> items returned in l_itm will have same order as given in the input list of attributes, l
    if preserve_order: lo = []
    for itm in dq.coll[secName].items:
        att = getattr(itm,attName)
        if att in l_att:
            l_itm += [itm]
            if preserve_order: lo += [l_att.index(att)]
    if preserve_order:
        l = zip(lo,l_itm)
        l.sort()
        l_itm = list(zip(*l)[1])
    return l_itm

def di_add_section_link(di, secName, d_link):
    # new version (12sep.18) that can follow more than just one link
                        
    secName1 = secName
    lt = []
    while secName1 not in ['CMORvar']:
        l = [t for t in d_link.keys() if t[1] == secName1]
        assert len(l) == 1, 'section name not found' #+ ' ' + secName1
        t = l[0]
        lt += [t]
        secName1 = t[0]
    lt = lt[::-1]
    
    if len(lt) > 0:
        for t in lt:
            secName0 = t[0]
            secName1 = t[1]
            if secName1 not in di:
                # Add a link in di to the required section
                att0 = d_link[(secName0, secName1)]
                itm0 = di[secName0]
                uid = getattr(itm0, att0)
                itm = dq.inx.uid[ uid ]
                assert itm._h.label == secName1
                di[secName1] = itm
        #print lt                                
        #if len(lt) > 1: stop

def di_add_grids(di, d_link):
    '''
    Also add in links to the items in dq.coll['grids'] that are pointed at by other sections associated with the CMORvar item.
    These differ slightly from the others in that a single item can point at more than one grids item.
    We'll store them in a dict, di['grids'], keyed by the uid's of the grid items. 
    The dq.coll['grids'] items have descriptive uids ('dim:longitude', etc) so this is an human-readable way to access the info.
    '''
    
    secName0 = 'grids'
    di[secName0] = {}
    k = secName0 + ' order'
    assert k not in di
    di[k] = []
    secName0_order = k
    '''
    Keep track of grids order because json tables store dimension info as a string with the dimensions in a consistent order,
    e.g. dimensions='longitude latitude olevel time'. It doesn't even appear as, say, 'time latitude longitude olevel'.
    So we need to know the order to reconstruct the dimensions string using info in the dreq. 
    In other words, don't change the order of list 'lt', below.
    '''
    lt = []
    lt += [('spatialShape', 'dimids')]
    lt += [('structure', 'dids')]
    lt += [('temporalShape', 'dimid')]
    lt += [('structure', 'cids')]
    
    l_uid = []
    for t in lt:
        secName, att = t
        di_add_section_link(di, secName, d_link)
        itm = di[secName]
        uid = getattr(itm, att)
        # uid could be a single unique id, or a tuple or list of them

        l = []
        if isinstance(uid, (tuple, list)):
            l = list(uid)
        elif isinstance(uid, (str, unicode)):
            l = [uid]
            
        if True:
            '''
            Check that order of dims from the 'grids' section items matches that
            given in the 'dimensions' attribute of the spatialShape or temporalShape item
            (these are the only sections in which a 'dimensions' attribute appears).
            
            I put this check here because in dreq version 01.00.28 there are two variables 
            (zhalfo_Oclim and zhalfo_Omon) for which the 'dimensions' string specifies half
            levels (olevhalf) but the corresponding grids item is for full levels ('olevel'). 
            Presumably this is a dreq error and the grids item should be 'dim:olevhalf'
            rather than 'dim:olevel'.
            '''
            if secName in ['spatialShape', 'temporalShape']:
                l_itm_grid = [dq.inx.uid[uid] for uid in l]
                s = getattr(di[secName], 'dimensions')
                dims  = [itm.label for itm in l_itm_grid]
                if s in [''] and len(dims) == 0:
                    pass # this is ok
                else:
                    # e.g. s = 'longitude|latitude'
                    dims2 = s.split('|')
                    if dims != dims2:
                        print 'dimension mismatch within dreq:  ',  di['CMORvar'].label, di['CMORvar'].mipTable, dims, dims2
            
        l_uid += l

    for uid in l_uid:
        assert uid not in di[secName0], uid
        di[secName0][uid] = dq.inx.uid[uid]
        di[secName0_order] += [uid]
        # Note, the uid's in the 'grids' section, unlike most other sections of the dreq, have human-readable names.
        # e.g. 'dim:latitude', 'dim:time', etc.
        

def ipr(itm):
    '''
    Print the attributes of an item. Skip the ones that begin with '_'.
    '''
    l = []
    m = 0
    if itm.__class__.__name__ in ['dict']:
        d = itm
    else:
        d = itm.__dict__
    for key in sorted(d.keys()):
        if key[0] in ['_']: continue
        l += [(key, d[key])]
        m = max(m,len(key))
    if True:
        # Add some extra bits to show.
        l0 = []
        for key in ['_h']:
            if hasattr(itm,key):
                l0 += [(key, getattr(itm,key))]
        l += l0
    indent = ' '*4
    w = ''
    for t in l:
        key = str(t[0])
        val = str(t[1])
        w += indent + key + ' '*(m - len(key)) + ' : ' + val + '\n'
    print w    

###############################################################################
if __name__ == "__main__":

    if compare_to_dreq:
        # Load the dreq
        
        # Ensure no other paths to dreq are in sys.path
        sys.path = [s for s in sys.path if os.path.basename( os.path.normpath(s) ) not in ['dreqPy']]
        # Ensure path to dreqPy is available
        assert os.path.exists(dreq_path)
        # Add correct dreqPy path to sys.path
        sys.path = [dreq_path] + sys.path
        import dreq
        try:
            dq # if already loaded, don't reload it (useful if running within ipython)
        except:
            dq = dreq.loadDreq()
        dreq_loaded = os.path.split(dreq.__file__)[0]
        # Ensure we loaded the right one
        assert dreq_path == dreq_loaded, dreq_path + ' expected, ' + dreq_loaded + ' loaded'
        print 'Using dreq in: '  + dreq_path
        print 'Version attribute of dreq module: ' + dreq.version
        print
        

    # CMOR tables to compare (e.g. Amon, CFday, Omon, ...)
    # Also referred to as "MIP tables", which is how dreqPy refers to them.
    # In pycmor we've been calling them "CMOR tables", so I'll use that term here.
    l_cmortable = sorted([itm.label for itm in dq.coll['miptable'].items], key=str.lower)
    #l_cmortable = l_cmortable[:2] # testing
        
    # Load the CMIP6 json tables.     
    d_table = {}
    path_cmortables = os.path.join(ncconv_dir, 'cmip6-cmor-tables/Tables')

    if test1: 
        if dreq_version in ['01.00.27']:
            path_cmortables = os.path.join('/HOME/rja/code/cmip6/cccma/env/CMIP6_work', 'cmip6-cmor-tables__01.00.27/Tables')
        elif dreq_version in ['01.00.28']:    
            path_cmortables = os.path.join('/HOME/rja/code/cmip6/cccma/env/CMIP6_work', 'cmip6-cmor-tables__01.00.28/Tables')
        elif dreq_version in ['01.00.29']:
            path_cmortables = os.path.join('/HOME/rja/code/cmip6/cccma/env/CMIP6_work', 'cmip6-cmor-tables__01.00.29/Tables')
        else:
            assert False, dreq_version
            
        #path_cmortables = os.path.join('/HOME/rja/code/cmip6/cccma/env/CMIP6_work', 'cmip6-cmor-tables__01.00.29/Tables')
        #path_cmortables = os.path.join('/HOME/rja/code/cmip6/cccma/env/CMIP6_work', 'cmip6-cmor-tables__01.00.27/Tables')

    print 'Using CMOR tables in: '  + path_cmortables
    ext = '.json'
    for cmortable in l_cmortable:
        # Get json file contents as a python dict
        tablepath = os.path.join(path_cmortables, 'CMIP6_' + cmortable + ext)
        if not os.path.exists(tablepath):
            print 'Table not found: ' + cmortable
            continue
        with open(tablepath, 'r') as f:
            d = json.load(f)
            #print 'Loaded contents of table: ' + cmortable
        #assert cmortable not in d_table
        d_table.update({cmortable : d})
    l_cmortable = [s for s in l_cmortable if s in d_table]
    
    if not True:
        l1 = os.listdir(path_cmortables)
        l2 = ['CMIP6_' + s + ext for s in l_cmortable]
        l3 = sorted([s for s in l1 if s not in l2])
        print 'Files in CMOR tables dir not associated with a CMOR table of variables:'
        for s in l3: print ' '*4 + s

    # Get list of attributes for variables in the json tables
    k = 0
    for cmortable in l_cmortable:
        d = d_table[cmortable]['variable_entry']
        l_cmorvar = sorted([str(s) for s in d.keys()], key=str.lower)
        d_table[cmortable]['l_cmorvar'] = l_cmorvar # for convenience save the list for re-use later
        for cmorvar in l_cmorvar:
            l_att = sorted([str(s) for s in d[cmorvar].keys()], key=str.lower)
            if k == 0: 
                l_att0 = l_att
            else: 
                #assert l_att == l_att0
                l_att0 += [s for s in l_att if s not in l_att0]
            k += 1

    l_att_json = sorted(l_att0, key=str.lower)
    del l_att, l_att0


    if compare_to_dreq:
        # For each variable attribute given in the json tables, find the corresponding information in the dreq. 

        if not True:
            for att in l_att_json:
               print att, find_attribute(att)

        # Check that version info for dreq and json tables agrees
        dreq_version = dreq.version
        fail_on_version_mismatch = not True
        json_version0 = None
        for cmortable in l_cmortable:
            json_version = str(d_table[cmortable]['Header']['data_specs_version'])
            if json_version0 is None: json_version0 = json_version
            assert json_version == json_version0 # all json files should be for same version of the dreq
        if fail_on_version_mismatch:
            assert dreq_version == json_version, 'Version mismatch for CMOR table ' + cmortable + ': ' + dreq_version + ' for dreq, ' + json_version + ' for json'
            print '\nUsing dreq version: ' + dreq_version + '\n'
        else:
            print '\nComparing dreq version ' + dreq_version + ' to json tables made for dreq version ' + json_version + '\n'
        
        # Check that the combination of CMOR table name and CMOR variable name (the short name, e.g. 'tas', 'zos', etc) 
        # uniquely identifies CMORvar items in the dreq. 
        tvl = ('mipTable', 'label')
        assert checkUnique('CMORvar', tvl, dq), 'Attributes do not uniquely identify CMORvar items: ' + ', '.join(tvl) 

        # Create a dict mapping (cmortable,cmorvar) names to items in the dreq. 
        d_CMORvar = {tuple([getattr(itm,s) for s in tvl]) : itm for itm in dq.coll['CMORvar'].items}
       
        # Dict to indicate what links an item in one section to another.
        d_link = {
            ('CMORvar', 'var')              : 'vid' # attribute of 'CMORvar' item that links to a 'var' item.
        ,   ('CMORvar', 'miptable')         : 'mtid'

        ,   ('CMORvar', 'structure')        : 'stid'
        ,   ('structure', 'spatialShape')   : 'spid' # link to spatial shape
        ,   ('structure', 'temporalShape')  : 'tmid' # link to temporal shape
        ,   ('structure', 'cellMethods')    : 'cmid' # link to cell methods record

        #,   ('structure', 'grids')          : 'cids' # link to grids

        ,   ('qcranges', 'CMORvar')         : 'vid'  # attribute of 'qcranges' item that links to (points at) a 'CMORvar' item
        }

        # Dict mapping names of attributes (as found in the json table) to sections & attribute names in the dreq. 
        # Note, the CMOR variable name, 'cmorvar' in this script, is ('CMORvar', 'label') in the dreq.
        d_att = {
            # json name                     way to find same info in the dreq
            # ---------                     ---------------------------------
            
            'cell_measures'                 : ('structure', 'cell_measures')
        ,   'cell_methods'                  : ('structure', 'cell_methods') 

        #,   'comment'                       : ('CMORvar', 'description')
        ,   'comment'                       : ('var', 'description')

        ,   'spatial dimensions'            : ('spatialShape', 'dimensions') # note, spatialShape items contain various info, e.g. info on the vertical levels
        ,   'temporal dimension'            : ('temporalShape', 'dimensions') # is this always just 'time'? other info is in the temporalShape item, e.g. if is a time mean

        ,   'frequency'                     : ('CMORvar', 'frequency')

        ,   'long_name'                     : ('CMORvar', 'title')    # ('var', 'title') may be same thing - always? 

        ,   'modeling_realm'                : ('CMORvar', 'modeling_realm')    

        ,   'ok_max_mean_abs'               : ('qcranges', 'ok_max_mean_abs')
        ,   'ok_min_mean_abs'               : ('qcranges', 'ok_min_mean_abs')

        #,   'out_name'                      : ('CMORvar', 'label')
        ,   'out_name'                      : ('var', 'label') # this is usually same as ('CMORvar', 'label'), but not always
        
        ,   'positive'                      : ('CMORvar', 'positive')

        ,   'standard_name'                 : ('var', 'sn')

        ,   'type'                          : ('CMORvar', 'type')

        ,   'units'                         : ('var', 'units')

        ,   'valid_max'                     : ('qcranges', 'valid_max')
        ,   'valid_min'                     : ('qcranges', 'valid_min')


            # The flag_meanings and flag_values attributes seem to occur for just one variable in the whole request!
            # The other 2062 CMOR variables seem to all share the same attributes in the json tables.
        ,   'flag_meanings'                 : ('structure', 'flag_meanings')
        ,   'flag_values'                   : ('structure', 'flag_values')
        }
        '''
        potentials:
        
    cell_measures [u'structure']
    cell_methods [u'cellMethods', u'structure']
    comment [u'miptable', u'experiment', u'qcranges', u'requestLink']
    dimensions [u'spatialShape', u'temporalShape']
    flag_meanings [u'structure']
    flag_values [u'structure']
    frequency [u'miptable', u'CMORvar']
    long_name []
    modeling_realm [u'CMORvar']
    ok_max_mean_abs [u'qcranges']
    ok_min_mean_abs [u'qcranges']
    out_name []
    positive [u'grids', u'CMORvar']
    standard_name []
    type [u'grids', '__main__', u'modelConfig', '__core__', u'CMORvar', u'timeSlice']
    units [u'grids', u'var', u'standardname']
    valid_max [u'grids', u'qcranges']
    valid_min [u'grids', u'qcranges']
        
        '''


        # Find links that map to CMORvar items, for cases where CMORvar item itself doesn't link to some related info, but instead the link goes the other way. 
        # Make a dict that presents that relation in a more intuitive format, as a mapping from the CMORvar item to the qcranges item.
        # It's more intuitive because in this script we're using CMORvar items as the basis for finding all information about variables.
        d = {}
        secName1 = 'CMORvar'
        for t in d_link:
            if t[1] not in [secName1]: continue
            secName0 = t[0]
            att = d_link[t]
            t = t[::-1]
            d[t] = {}
            for itm in dq.coll[secName0].items:
                uid = getattr(itm, att) # uid = id of the secName1 item
                assert uid not in d
                d[t][uid] = itm.uid # itm.uid = id of the secName0 item
        d_itm_links = d       
        #for t in d_itm_links: print t; print d_itm_links[t].items[0]
        
        d_mismatch = {att : [] for att in l_att_json}
        d_missing  = {att : [] for att in l_att_json}
        
        # Loop over CMOR tables and CMOR variables
        nvar = 0
        ddi = {}
        for cmortable in l_cmortable:
            l_cmorvar = d_table[cmortable]['l_cmorvar']
            nvar += len(l_cmorvar)
            for cmorvar in l_cmorvar:
                ti = (cmortable, cmorvar)
                if ti not in d_CMORvar: continue
                di = {'CMORvar' : d_CMORvar[ti]}
                di_add_grids(di, d_link)
                
                dr = {}

                for t in d_itm_links:
                    d = d_itm_links[t]
                    secName0 = t[0]
                    secName1 = t[1]
                    assert secName0 in ['CMORvar']
                    if secName1 not in di:
                        uid = di[secName0].uid
                        if uid in d: 
                            di[secName1] = dq.inx.uid[ d[uid] ]
                        else:
                            di[secName1] = None

                for att_json in l_att_json:
                    if att_json in d_att:
                        secName, att = d_att[att_json]
                        if secName not in di: di_add_section_link(di, secName, d_link)
                        itm = di[secName]
                        if hasattr(itm,att): dr[att_json] = getattr(itm,att)
                    elif att_json in ['dimensions']:
                        # combine spatial & temporal dimension info from the dreq into a single string describing the dimensions
                        # e.g. 'longitude latitude time'
                        if True:
                            # This seems to be the correct way to generate the 'dimensions' string found in the json tables
                            # using the dreq info. 
                            
                            l_itm_grid = [di['grids'][dn] for dn in di['grids order']]
                            dims = [itm.label for itm in l_itm_grid]

                            # or is itm.standardName of the 'grids' item what I should use?
                            # no, itm.label seems more specific. e.g. itm.label = 'height2m' while itm.standardName = 'height'
                            
                            dr[att_json] = ' '.join(dims)
                            
                        else:
                            '''
                            This is how I generated the 'dimensions' string for the google spreadsheet up until this point (23jan.19).
                            It doesn't match all the json table entries, for these reasons:
                            - order of dimensions must be the same
                            - scalar dimensions, e.g. 'height2m', should also be included
                            - other "dimensions" besides space & time should be included, e.g. spectral space
                            Should probably fix how it appears in the google spreadsheet. (23jan.19)
                            '''
                            assert False  # because of the json mismatch, don't use this method
                        
                            lt = [d_att[s] for s in ['spatial dimensions', 'temporal dimension']]
                            l = []
                            for t in lt:
                                secName, att = t
                                if secName not in di: di_add_section_link(di, secName, d_link)
                                l += [ getattr(di[secName],att) ]
                            l = [s.replace('|',' ') for s in l]
                            dr[att_json] = ' '.join(l).strip()
                            
                    else:
                        assert False, 'Unknown json table attribute: ' + att_json
                        
                    if att_json in ['comment']:
                        if att_json in dr:
                            s = dr[att_json]
                            dr[att_json] = s.replace('"', '\'') # double vs single quotes causes a lot of mismatches between comment strings
                   
                dj = dict(d_table[cmortable]['variable_entry'][cmorvar])                    
                    
                dj = {str(t[0]) : str(t[1]) for t in dj.items()} # convert unicode to string

                # remember info in one place in case want to use it later
                ddi[(cmortable, cmorvar)] = {'di' : di, 'dr' : dr, 'dj' : dj}

                
                # Compare dr (info from the dreq) to dj (info from the json tables)
                for att in l_att_json:
                    if att not in dj: 
                        # attribute isn't in the json table
                        continue
                    elif att not in dr: 
                        # attribute is in the json table, but not in the dreq
                        #d_missing[att]  += [cmorvar]
                        d_missing[att]  += [ {'cmorvar' : cmorvar, 'cmortable' : cmortable, 'di' : di, 'json' : dj[att]} ]
                    elif dj[att] != dr[att]: 
                        # attribute is in both, but doesn't agree between them
                        #d_mismatch[att] += [cmorvar]
                        d_mismatch[att] += [ {'cmorvar' : cmorvar, 'cmortable' : cmortable, 'di' : di, 'json' : dj[att], 'dreq' : dr[att]} ]


        print '-'*80
        print
        print 'Total no. of variables in json tables: ' + str(nvar)
        print 'Total no. of CMOR variables in dreq:   ' + str(len(dq.coll['CMORvar'].items))


        d_occurrence = {att: [] for att in l_att_json}
        for cmortable in l_cmortable:
            l_cmorvar = d_table[cmortable]['l_cmorvar']
            for att in l_att_json:
                l = [s for s in l_cmorvar if att in d_table[cmortable]['variable_entry'][s]]
                d_occurrence[att] += [ {'cmorvar' : s, 'cmortable' : cmortable} for s in l ]
                

        lt = []
        lt += [ ('occurrences', d_occurrence) ]
        lt += [ ('mismatches', d_mismatch) ]
        lt += [ ('missing attributes', d_missing) ]
        

        for t in lt:
            print '\nNo. of ' + t[0] + ' by json table attribute:'
            d = t[1]
            for att in l_att_json:
                if att not in d: continue
                n = len(d[att])
                print str('%-20s : %-4i' % (att, n)) + '  (' + str('%.1f' % float(100.*n/nvar)) + '%)'
            l = [att for att in d if len(d[att]) > 0]
            if t[0] not in ['occurrences']:
                print 'Nonzero number of ' + t[0] + ': ' + ', '.join( sorted(l, key=str.lower) )

        print '-'*80

        if True:
            '''
            For comments, it seems that the json ones match what's in the dreq, but are sometimes truncated.
            That is, the json comment is a shorter version of the dreq comment. 
            It seems that json comments are no longer than 1023 characters (in 01.00.28 dreq version).
            Comments in the dreq that are longer than this get truncated.
            Dunno why.  (23jan.19)
            '''
            att_json = 'comment'
            if len(d_mismatch[att_json]) > 0:
                print '\nCheck mismatches on json attribute: ' + att_json
                dc = {
                    'json is subset of dreq'    : 0
                ,   'dreq starts with json'     : 0
                ,   'dreq is subset of json'    : 0
                ,   'complete mismatch'         : 0
                }
                for d in d_mismatch[att_json]:
                    if d['json'] in d['dreq']:
                        dc['json is subset of dreq'] += 1
                        if d['dreq'].startswith(d['json']):
                            dc['dreq starts with json'] += 1
                            #print d['dreq'].partition(d['json'])[-1]
                    elif d['dreq'] in d['json']:
                        dc['dreq is subset of json'] += 1
                    else:
                        dc['complete mismatch'] += 1
                print dc

                print 'For mismatches:'
                print '  max length of ' + att_json + ' in dreq:', max([len(d['dreq']) for d in d_mismatch[att_json]])            
                print '  max length of ' + att_json + ' in json:', max([len(d['json']) for d in d_mismatch[att_json]])
                
                print 'For all variables:'
                l = []
                for cmortable in l_cmortable:
                    l_cmorvar = d_table[cmortable]['l_cmorvar']
                    l += [ len( d_table[cmortable]['variable_entry'][cmorvar][att_json] ) for cmorvar in l_cmorvar ]
                print '  max length of ' + att_json + ' in json:', max(l)
        
        if True:
            l = []
            l += ['ok_max_mean_abs']
            l += ['ok_min_mean_abs']
            l += ['valid_max']
            l += ['valid_min']
            l_att = l
            for att_json in l_att:
                print '\nCheck mismatches on json attribute: ' + att_json
                dc = {
                    'json is blank, dreq is a number' : 0
                ,   'json is blank, dreq is a placeholder' : 0
                }
                for d in d_mismatch[att_json]:
                    if d['json'] in ['']:
                        if isinstance(d['dreq'], (float, int)):
                            dc['json is blank, dreq is a number'] += 1
                        elif isinstance(d['dreq'], dreq.dreqItemBase):
                            dc['json is blank, dreq is a placeholder'] += 1
                print dc


    ###############################################################################
    validate_coordinates = True
    if validate_coordinates:
        '''
        The file CMIP6_coordinate.json does not indicate its data request version! 
        The only indication of its version is that it resides in the same dir as the 
        other CMIP6 tables. 
        Check here that the info in this file is consistent with that in the dreq's 
        'grids' section.
        
        CMIP6_coordinate.json isn't the only file in the tables dir that isn't associated
        with CMOR tables of variables. The non-variable json files are:
            CMIP6_CV.json
            CMIP6_coordinate.json
            CMIP6_formula_terms.json
            CMIP6_grids.json
        The CV one has details on modelling centres & experiments.
        I'm not sure if the _grids & _formula_terms ones have corresponding info in the 
        data request. (26jan.19)
        '''
        cmortable = 'coordinates'
        print '-'*80
        print '\nChecking ' + cmortable + ' json table against dreq info:\n'

        cmortable = 'coordinate'
        tablepath = os.path.join(path_cmortables, 'CMIP6_' + cmortable + ext)
        with open(tablepath, 'r') as f:
            d = json.load(f)
        d_table.update({cmortable : d})
        
        dr_grids = {itm.label : itm.__dict__ for itm in dq.coll['grids'].items}
        dj_grids = d_table[cmortable]['axis_entry']
        nvar = len(dj_grids)

        l_same_name = ['axis', 'positive', 'requested', 'type', 'units', 'value']
        d_grid_att = {
          # json name           : dreq name
            'bounds_values'     : 'boundsValues'
        ,   'must_have_bounds'  : 'bounds'
        ,   'out_name'          : 'altLabel'
        ,   'requested_bounds'  : 'boundsRequested'
        ,   'standard_name'     : 'standardName'
        ,   'stored_direction'  : 'direction'
        ,   'tolerance'         : 'tolRequested'
        }
        d_grid_att.update({s : s for s in l_same_name})
        l_att_json = sorted(d_grid_att.keys())
        
        d_mismatch = {att : [] for att in d_grid_att}
        d_missing  = {att : [] for att in d_grid_att}
        
        l_gn_json = sorted(dj_grids.keys()) # list of grid names appearing in the json file
        l_gn_dreq = sorted(dr_grids.keys())
        l_gn = chksme(l_gn_json + l_gn_dreq)

        for gn in l_gn:
            if   (gn in l_gn_json) and (gn not in l_gn_dreq):
                print 'in json but not in dreq: ' + gn
            elif (gn in l_gn_dreq) and (gn not in l_gn_json):
                print 'in dreq but not in json: ' + gn
            else:
                dj = {s : dj_grids[gn][s]             for s in l_att_json if s in dj_grids[gn]}
                dr = {s : dr_grids[gn][d_grid_att[s]] for s in l_att_json if d_grid_att[s] in dr_grids[gn]}
                k = 'requested'
                if k in dr:
                    if len(dr[k]) > 0:
                        dr[k] = dr[k].split()
                k = 'requested_bounds' 
                if k in dr: 
                    dr[k] = [str(m) for m in dr[k]]
                k = 'bounds_values'
                if k in dr: 
                    dr[k] = ' '.join([str('%.1f' % m) for m in [float(s) for s in dr[k].split()]])
                for att in l_att_json:
                    if att not in dj:
                        continue
                    elif att not in dr:
                        if dj[att] not in ['']: # if attribute is blank then it doesn't matter that it wasn't found in the dreq
                            d_missing[att] += [{'gn' : gn, 'dj' : dj[att]}]
                    elif dr[att] != dj[att]:
                        d_mismatch[att] += [{'gn' : gn, 'dj' : dj[att], 'dr' : dr[att]}]

        lt = []
        lt += [ ('mismatches', d_mismatch) ]
        lt += [ ('missing attributes (with non-empty values)', d_missing) ]
        
        for t in lt:
            d = t[1]
            #if not any([len(d[s]) > 0 for s in d]): continue
            print '\nNo. of ' + t[0] + ' by json table attribute:'
            for att in l_att_json:
                if att not in d: continue
                n = len(d[att])
                print str('%-20s : %-4i' % (att, n)) + '  (' + str('%.1f' % float(100.*n/nvar)) + '%)'
            l = [att for att in d if len(d[att]) > 0]
            if t[0] not in ['occurrences']:
                print 'Nonzero number of ' + t[0] + ': ' + ', '.join( sorted(l, key=str.lower) )

        for t in lt:
            d = t[1]
            if not any([len(d[s]) > 0 for s in d]): continue
            print '\nNames of grids with ' + t[0] + ', by json table attribute:'
            for att in l_att_json:
                if len(d[att]) == 0: continue
                print str('%-20s : ' % att) + ', '.join([d1['gn'] for d1 in  d[att]])
     

        if not True:
            l = []
            for gn in l_gn_json:
                dj = dj_grids[gn]
                k = 'generic_level_name'
                if k in dj:
                    if dj[k] not in ['']:
                        print str('%-30s %s' % (gn, dj[k]))
            

    ###############################################################################
    if show_dreq_grids_info:

        print '\ndreq info on grids'    
        l_itm = dq.coll['grids'].items
        print 'No. of grids items, total:', len(l_itm)
        print 'No. of grids items, by axis type:'
        l_ax = sorted( chksme([itm.axis for itm in l_itm]) )
        d_itm_by_ax = {}
        for ax in l_ax:
            d_itm_by_ax[ax] = [itm for itm in l_itm if itm.axis == ax]
            s = ax
            if ax == '': s = 'no axis given'
            print ' '*4 + str('%-13s' % s) + ' : ' + str( len(d_itm_by_ax[ax]) )
        print 'Names of grids, by axis type:'
        for ax in l_ax:
            s = ax
            if ax == '': s = 'no axis given'
            #print ' '*4 + 'uid,label for axis: ' + s
            print ' '*4 + 'label for axis: ' + s
            for itm in d_itm_by_ax[ax]:
                #print ' '*8 + str('%-20s %-20s' % (itm.uid, itm.label))
                print ' '*8 + itm.label
                #assert itm.uid == 'dim:' + itm.label
                if itm.uid != 'dim:' + itm.label: 
                    print ' '*20 + 'uid = ' + itm.uid
        if checkUnique('grids', 'label', dq):
            print '\'label\' attribute uniquely identifies items in \'grids\' section'
        else:
            print '\'label\' attribute DOES NOT uniquely identify items in \'grids\' section'

        

    ###############################################################################






