
 .. warning::
    This section is specific to CMIP6-era netcdf conversions.
    It may be updated in future.
    Nightly builds are not currently active.


Nightly Builds
--------------

During the publication process for CMIP6, the development and use of :code:`pycmor`
will continue to change as needs arise to deal with failed conversions and problems
in the produced data. While these changes will hopefully only result in expected changes, 
it is possible that unexpected changes may occur. As a result of this, in order to 
be notified when unexpected changes occur, a nightly build, continuous integration, 
or CI, pipeline has been built for the :code:`pycmor` software. 

As part of these "nightly builds", which will occur every night, we run a year
of conversion on ``rc3.1-his01`` and ``canoe-his01`` for every variable in every table, 
using the "bleeding edge" version of :code:`pycmor` and automatically perform a detailed
analysis on the results by inspecting "variable hashes" in the resulting files.
In addition to this, we use `validate <https://gitlab.science.gc.ca/ncs001/validate>`_ 
to produce plots of these data, comparing the CanESM5 historical data to CanESM2, when possible.

At a high level, what these nightly builds aim to do is to notify us when
unexpected changes occur in the variable values. It does this by calculating an
integer hash associated with all the variable arrays in each converted file,
and comparing them to stored values in
:code:`CI/nightly_builds/var_hashes.json`. If changes
are noted, it simply provides a list of what variables were altered, but if
said variables have already been published (or "frozen"), then the nightly
build will be said to have failed. It should be noted that **if** variable
hashes are altered **for non-frozen** variables, then the nightly builds
automatically update and store these new hashes. However, if **frozen** variables
are altered, this automatic update of the stored hashes will not occur, and it
must be handled manually after addressing the cause of the hash update.
**It is at this stage that an user must dig into what caused the hash delta** -
this duty ultimately falls on the user tasked with `Monitoring the Nightly Build Pipelines`_.

To accomplish this, the nightly-build "pipeline" runs are part of the gitlab's
continuous integration workflow, using the hash checking technology
discussed above. More on these technologies, along with where to find the
stored working directory, variable tables, and resulting validate plots can be
found below.

* `The Pipeline`_
* `Hash Checking`_
* `Nightly Build Results`_

.. note::

    In this document we will mention the location of the main nightly build
    configuration files and scripts, as well as where the nightly builds store
    working directories and output data. These two locations are likely to be
    altered in the future, and thus we refer to them as :code:`$NB_SCRPT_DIR`
    and :code:`$NB_STRG_DIR`, respectively. As of 2019-10-21, these are set to

    .. code-block:: bash

        NB_SCRPT_DIR=CI/nightly_builds/ 
        NB_STRG_DIR=/space/hall2/sitestore/eccc/crd/ccrn/users/scrd108/pycmor_nightly_build

    where :code:`$NB_SCRPT_DIR` is a relative path within the :code:`ncconv` repo, and 
    :code:`$NB_STRG_DIR` is an abosolute path on :code:`ppp2`.


The Pipeline
^^^^^^^^^^^^
The nightly build pipelines can be found `here
<https://gitlab.science.gc.ca/CanESM/ncconv/pipelines>`_, and the pertinent
pipelines can be identifyed by locating those that were executed on the
:code:`nightly-builds` branch (in the "Commit" column), with a "triggered" tag.
If a user clicks on the status icon, on the leftmost column, they are brought
to a more detailed view of the stages associated with said pipeline; each stage
can be thought of as a column on this display, and within each column there is
one or more jobs associated with said stage.

With that said the nightly-build pipeline has been broken into 5 stages:

1. **Run Pycmor**

   * This stage is the main conversion step where :code:`pycmor` is actually ran

2. **Document Conversion**

   This stage has two jobs

   a. *conv-summary*: prints a nice numeric summary of the entire conversion,
      i.e. how many variables successfully converted, or how many failed due to
      errors in options decks, etc.
   b. *produce-plots*: using `validate <https://gitlab.science.gc.ca/ncs001/validate>`_, 
      produces simple plots for all converted variables and stores them on 
      `hpfx <http://hpfx.science.gc.ca/~scrd104/pycmor_nightly_builds/>`_ 

      .. note::

            As of 2019-10-21, the validate plots are not functional. This is a known
            issue and will be fixed when an opportunity arises.

3. **Summarize Table Conversions**

   * This stage has a "job" for each table which creates a more comprehensive
     summary of what occurred for said table. Specifically, these jobs show a
     list of variables that failed, and why.

4. **Check Deltas**

   * Like the 'Summarize Tables' stage, this one has a job for each CMOR table
     which simply checks the logs for hash changes. If any changes are noted,
     the altered variables are printed. **If a frozen** variable has changed,
     then after outputting what variables have been altered, they exit with a
     non-zero exit status, causing the job to fail, causing a nice red "X" to
     appear on the job. This will also halt the pipeline from proceeding to the
     next stage.

5. **Updated Nonfrozen hashes**

   * Provided this stage is executed, it check for any differences between the
     newly produced hash file and its stored counter part (described in the
     `Hash Checking`_ section), and updates the stored version
     if any non-frozen updates are noted. Note that this step will not be
     executed if any frozen updates are noted as the previous stage will have
     failed. **Updating the stored frozen hashes requires manual
     intervention**.

Hash Checking
^^^^^^^^^^^^^
It is not realistic to run the hash checking functionality for every year of
every experiment, for every variable, and thus it is turned off by default for
:code:`pycmor`. If a user wishes to turn on this functionality, they can do so
by running :code:`pycmor.py` with the :code:`--check_deltas` flag, which makes
:code:`pycmor` calculate (if applicable)/store hash information for every
variable seen in the tables of interest. The data is stored
the following dictionary format:

.. code-block:: javascript

    {
    "CanESM5-tas-Amon-rc3.1-his01-2005_2005": {
        "frozen": true, 
        "hash": 2700954587488220678, 
        "hash_prod_cmmt": "877d75fde79dd09108ac5c208600f2db9f01ce94"
        },

        ...
    }

where the :code:`${source_id}-${varname}-${CMORtable}-${runid}-${chunk}` represents a unique
"conversion key" for the data in question, :code:`hash` is the calculated
integer hash associated with the variable arrays (including dimensions),
:code:`hash_prod_cmmt` represents the :code:`ncconv` commit that produced said
variable hash, and :code:`frozen` is a boolean value that states whether the
:code:`hash` should be allowed to change or not. These integer hashes are
calculated are looping over all variables within a netcdf file, loading their
values and converting them into a string and then calculating a hash using
:code:`python`'s built in :code:`hash()` function. The resulting integer hashes
are then summed for all variables in the file.

If the hash checking functionality is used, when calculating the variable
hashes, :code:`pycmor` continually compares the results to stored version in
:code:`$NB_SCRPT_DIR/var_hashes.json`, and uses information in it to populate
the new dictionary. **If the calculated hash and the stored hash don't match**,
then :code:`hash_prod_cmmt` is set to the current :code:`HEAD` of
:code:`pycmor`, and the variable name is stored and logged into
:code:`pycmor_logs/CMORTABLE/hash_deltas.log`. Additionally, if the
:code:`frozen` equal :code:`true` in the stored file for said variable, then
the variable name is also logged in
:code:`pycmor_logs/CMORTABLE/frozen_hash_deltas.log`. **If no change is
noted**, then :code:`pycmor` adopts the :code:`hash_prod_cmmt` from the stored
version, and no additional logging it done. Note that the value of
:code:`frozen` is always inherited from the stored file, unless the conversion
key doesn't exist, in which case case, a fresh entry is created with
:code:`frozen` set to :code:`false`. At the end of the conversion process,
:code:`pycmor` creates a local :code:`var_hashes.json` file in the working
directory. 

Nightly Build Results
^^^^^^^^^^^^^^^^^^^^^

By design, these pipelines produce a lot of information that can be used get an
information on what happened during the conversion process. In addition to being
able to view the :code:`stderr` and :code:`stdout` for the executed scripts by
selecting the job of interest, they also store/produce the following:

1. **all produced logs and hash file**

   * Sometimes a user will need to manually inspect these files for more
     insight into what occured. As such, these files are automatically stored as
     "artefacts" from each pipeline, and as such can be downloaded via a button
     the very right of the pipeline page (there is an arrow, pointing down..
     for download). It should be noted that these artefacts are only accessible
     for a week after the pipeline has ran.  If you'd like to find these files
     **after** this deadline has been passed, you will need to navigate to the
     stored working directory for said pipeline (see below).

2. **the variable tables used in the conversion**

   * Prior to executing the conversion, in the :code:`run_pycmor` stage, the 
     convert job updates the variable json tables to match the current version 
     of the variable table spreadsheet. After doing this, it then stores the
     entire :code:`variable_tables` directory in :code:`${NB_STRG_DIR}/DATE-OF-BLD/`.

3. **the working directory contents for the conversion**

   * After the conversion, the convert job stores this in :code:`${NB_STRG_DIR}/DATE-OF-BLD/` 

4. **the produced netcdf files**

   * during the conversion process, :code:`pycmor` writes the output files to 
     :code:`${NB_STRG_DIR}/CMIP6`.

     .. note::
        
        Prior to each conversion, the netcdf files from previous nightly builds
        are removed. This is done so only files produced by the most recent 
        version of :code:`pycmor` are found here.

5. **validate figures from the resulting data**

   * a viewer has been setup on :code:`hpfx` to view the most recent nightly build plots. 
     Find it `here <http://hpfx.science.gc.ca/~scrd104/pycmor_nightly_builds/>`_

Monitoring the Nightly Build Pipelines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
As discussed above, the nightly builds are ran every night and the results can be directly 
monitored `here <https://gitlab.science.gc.ca/CanESM/ncconv/pipelines>`_. 

.. note:: 
    **whomever is tasked with monitoring the nightly builds should be checking this page every morning.**

Prior to getting into the details on how to monitor/trouble shoot the nightly builds, it is 
worth noting what triggers the pipeline, and where they get run:

1. First, ``scrd105`` triggers a pipeline to run on ``eccc1-ppp2.science.gc.ca`` via ``hcron`` 
   (specifically ``hcron1.science.gc.ca``) and an "installed" 
   `trigger script <https://gitlab.science.gc.ca/CanESM/ncconv/blob/pycmor/CI/nightly_builds/trigger-pycmor-nightly-build>`_
   in their ``~/bin`` directory, at 5:05 am (eastern).
2. The trigger clones down a copy of the ``ncconv`` repository into a temporary directory within 
   ``scrd105``'s home directory, merges the ``HEAD`` of the ``pycmor`` branch into the ``nightly-builds``
   branch, and pushes the merge up to ``gitlab.science``.
3. the trigger sends a signal to ``gitlab.science`` to make ``scrd108`` to run all 
   "jobs" defined in the ``.gitlab-ci.yml`` 
   `file <https://gitlab.science.gc.ca/CanESM/ncconv/blob/pycmor/.gitlab-ci.yml>`_ with the::

        only:
            - triggers
   clause, on the updated ``nightly-builds`` branch.
   
With this in mind, it is useful to have a brief understanding of what a "job" is. Each job is defined by each 
individual ``yaml`` block with in ``.gitlab-ci.yml`` file, i.e. the ``convert`` job is defined 
with-in the::

    convert:     
        stage: run_pycmor
        ...
        ...
        only:
            - triggers
block. Furthermore, each of these job blocks corresponds to one of the jobs described in
the `The Pipeline`_ section, where the ``stage:`` field is used to define what column
the job belongs to in the pipeline view (see `here <https://gitlab.science.gc.ca/CanESM/ncconv/pipelines/8877>`_
for an example). 

During each job, the runner first executes the code in the ``before_script:`` portion of ``.gitlab-ci.yml``, and 
*then* runs the code in the ``script:`` portion of the job of interest. It is worth noting that the 
working directory is not maintained between jobs - the only thing that is passed to following jobs are those defined 
in the ``artifacts:`` section of a job block.

For further information on what each field/block means within the ``.gitlab-ci.yml``, users are encouraged to
review the `gitlab.science CI/CD help page <https://gitlab.science.gc.ca/help/ci/README.md>`_. 

What to do if a pipeline fails
""""""""""""""""""""""""""""""
When a pipeline fails, depending on where you are looking, you will **see some form of a red X**. If you are on the 
`Pipelines page <https://gitlab.science.gc.ca/CanESM/ncconv/pipelines>`_ you will "failed" badge in the "Status" 
column, along with a red X in the "Stages" column, which will give you a rough idea of where the pipeline failed, i.e.

.. figure:: imgs/Failed_Pipeline.png
    :align: center

To determine exactly where the failure occurred, you will need to navigate to the more detailed pipeline viewer, by clicking
the "Status" badge for the pipeline of interest, which in the above example is the "failed" badge - this will take you
to a page similar to that below:

.. figure:: imgs/Pipeline_View.png
    :align: center

Now, in this view, you should be able to locate exactly which job failed - once you have this, click on the jobs badge
to navigate to view port that shows the jobs ``stdout/stderr``:

.. figure:: imgs/JobView.png
    :align: center

It is in this view you should see *why* the pipeline failed - refer to what is laid out in the ``script:`` section of the 
job block to see what is being executed. 

Failures can essentially be lumped into one of three categories:

1. weird system problems
2. a bug was brought into the code base that caused the code to break entirely
3. a change was brought into the code base such that a "frozen" variable saw an unexpected 
   variable hash update, and a job in the "Check Deltas" stage threw a non-zero exit 
   status.

For the first category, the user can either manaully launch *another* pipeline (which requires access to the ``scrd105`` account), 
or wait until another pipeline launches the next morning - if it is *truly* just a system problem, it shouldn't happen again.

For the second category, the user will have to implement standard debugging practices, and eventually have the fix merged into
the ``pycmor`` branch. 

.. note:: 

    If a user would like to inspect the working directory, and the convert job managed to 
    complete, the user should be able to find a stored version of the working directory stored at ``${NB_STRG_DIR}/DATE-OF-BLD/``, 
    where ``NB_STRG_DIR`` is defined in the nightly builds
    `config file <https://gitlab.science.gc.ca/CanESM/ncconv/blob/pycmor/CI/nightly_builds/nb_config.sh>`_. If the convert stage did
    not complete, the original working directory *may* be available within one of the directories beneath ``~scrd108/builds/``, 
    but it will be harder to find as these directories are used for the working directories for the CI across multiple projects.

For the third category, fixing the problem can be a bit more nuanced, and the methods are described in the section
directly below.

What to do if you see a frozen hash update?
"""""""""""""""""""""""""""""""""""""""""""
When a frozen hash update occurs, the user must determine if:

a. the hash update was caused by a true data *correction*, or
b. the hash update was caused by either a bug being merged into
   ``pycmor`` or a mistake was introduced into the CCCma 
   variable tables

If the answer is (a), then the user needs to update the stored hash `file <https://gitlab.science.gc.ca/CanESM/ncconv/blob/pycmor/CI/nightly_builds/var_hashes.json>`_,
``CI/nightly_builds/var_hashes.json``. To do this, the user will need to navigate to ``${NB_STRG_DIR}/DATE-OF-BLD/``, 
grab the newly produced hash file and use it to update the file on the ``nightly-builds`` branch.

.. note::

    The ``var_hashes.json`` file contained on ``nightly-builds`` branch is considered the "true" version, and 
    may quite different than that contained on the ``pycmor`` branch. This is because the nightly builds pipeline continually
    updates the variable hashes for non published variables. **Therefore, when updating the hash file, it must be 
    committed and pushed to the ``nightly_builds`` branch**

If the answer is (b), the user will need to figure out why the change was seen - a good starting place is to determine if
the variable was actually converted. If the variable conversion *did fail*, then this should be able to be seen in the table summary
job, in the "Summarize Table Conversions" stage. 

If the variable did fail, the table summary stage should provide a rough idea as to why, but the user should inspect the log files for the 
conversion to determine exactly why (which are described in detail in the "Inspecting the Log Files" page) - these log files can be 
accessed one of two ways:

a. navigate to the stored working directory at ``${NB_STRG_DIR}/DATE-OF-BLD/``
b. download the "artifacts" from this conversion job, which can be found navigating to the convert job's ``stdout/stderr`` page
   and hitting the "Browse" or "Download" button, under the "Build artifacts" heading on the right hand side of the page

If the variable *didn't* fail, things involve a bit more detective work, but a good place to start is to look if there have 
been any notable variable table changes recently, or if optional decks/the ``pycmor`` source code has changed. Note that 
to inspect the *exact* variable tables that were used, the user will need to nagivate to ``${NB_STRG_DIR}/DATE-OF-BLD/``

.. note::

    The nightly builds use the **MOST CURRENT** version of the 
    `variable tables <https://docs.google.com/spreadsheets/d/1JLsCcp9CCP-39ZE5OCN0xaerMwLe4EvmYj-a4-6KWjM/edit>`_
    as it pulls down the new version of them prior to running the conversion in the "convert" stage.
    A good way to see what has changed in the tables recently is to run a ``diff`` command between the 
    used variable tables in subsequent nightly build pipelines (contained at ``$NB_STRG_DIR``)

What to do if pipeline is hanging indefinitely
""""""""""""""""""""""""""""""""""""""""""""""
Occasionally, system problems may cause a pipeline to hang indefinitely, even though the CI/CD time limit 
for this project is 3 hours. At the moment, it is not clear as to why this happens, but in general the pipelines take
2-3 hours to complete, so if one is going for a significantly longer time, than the user should manually cancel via
the red X below

.. figure:: imgs/ExampleCancelButton1.png
    :align: center

or the cancel button on the more detailed pipeline viewer:

.. figure:: imgs/ExampleCancelButton2.png
    :align: center

Once you cancel a pipeline, you should see something similar to 

.. figure:: imgs/CanceledPipeline.png
    :align: center
