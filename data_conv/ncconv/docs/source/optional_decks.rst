
Optional Decks
--------------
As part of :code:`pycmor`, there is a fair amount of file management and data manipulation that 
occurs. While it is interworking :code:`python` modules that create the entire pipeline, executable
programs called "optional decks" do most of the data manipulation. These programs are used for a 
wide range of purposes, from simple things like extracting a single level from a 3D field, adding
two variables together, or multipying a whole field by a constant, to more complex things like 
vertical integrations. 

These optional decks get called from within :code:`pycmor` automatically, using the information provided
in the `CCCma tables <setup_run.html#variable-tables>`_
and thus :code:`pycmor` expects specific formats for the information in the CMOR table, along with
how the optional decks get called. This page lays out those rules. 

.. warning:: 
    
    In addition to following these guidelines, optional decks must be executable from the command 
    line. If they are not, users will see a :code:`Permision denied` in the :code:`optional_deck_errors.log`.

Input Arguments
^^^^^^^^^^^^^^^

Input variables in a single diag filetypes
""""""""""""""""""""""""""""""""""""""""""
When optional decks only need to operate on variables from a single diag file
(from :code:`CCCma diag file` field),  it must be capable of being called like:

.. code-block :: bash
    
    myfaveoptdeck pfx invar out

where :code:`pfx` represents a prefix (which contains diag file information) that
when combined with the input variable name, :code:`invar`, defines the input file,
and when combined with :code:`out`, defines the output file. i.e: :code:`${pfx}_${invar}`
or :code:`${pfx}_${out}`. If the deck must operate on n variables from the same diag
file, the deck should accept the arguments as

.. code-block :: bash

    myfaveoptdeck pfx invar1 invar2 ... invarn out

Input variables contained in multiple diag filetypes
""""""""""""""""""""""""""""""""""""""""""""""""""""
When optional decks must operate on variables from multiple diag file types,
it must take in arguments in the following format

.. code-block :: bash

    myfaveoptdeck infile1 invar1 infile2 invar2 ... infilen invarn out

where in this case, :code:`infilen` defines the full name of the nth input
file, :code:`invarn` the nth variable name contained in the file, and :code:`out` is the
output filename. 

Input/Output File Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In addition to following the above mentioned input argument requirements,
internal to the optional decks, they must follow the following standards to be
consistent with assumptions inherent to :code:`pycmor`:

1. **the output file must contain only a single data variable that corresponds
   to the requested CMOR variable** 
2. for optional decks on the ocean side, internal to netcdf file resulting from 
   the optional deck, **the non-dimensional variable name should be set to the
   FIRST variable name in its argument list** (i.e. :code:`invar` or
   :code:`invar1`), even though this may not actually be the true name of the
   derived variable name. This is required so the module that handles the
   conversion of ocean variables, ``nmor.py``, knows what variable name to 
   expect.

Additional Considerations
^^^^^^^^^^^^^^^^^^^^^^^^^
- when setting multiple values in :code:`CCCma diag file` or :code:`CCCma TS var name`,
  the lists must be separated by spaces. 

  .. warning::

    **Order matters**: when inputting these values into the CCCma tables, make
    sure that the order given in the table fields match the order of which the 
    specified optional deck expects them. As some operations require a specific order
    of operations, i.e. division, or subtraction.

- if the optional deck is to be applied to ocean fields and it requires a mesh mask file,
  **do not access a mask file within the optional deck**. To make sure that the same 
  mask file is used between the ``nmor.nemo_to_cmor`` code and the optional decks, the
  ``table_utils.run_optional_deck`` function links the file accessed with-in the
  ``table_utils.convert_table`` function into the optional deck work-space, with the 
  name ``nemo_mesh_mask``. **Therefore the desired mesh mask file will already be present,
  and you only need to use the correct name**.
