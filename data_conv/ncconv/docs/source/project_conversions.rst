Project-Specific Conversions
------------------------------------

Netcdf conversions can be done using CMOR tables defined by different projects. 
The :code:`project` parameter specifies which set of tables to use.
To accommodate new variable conversions, new projects can be added by following the instructions below.
A new project would often use "official" CMOR tables provided by some external project (e.g., CMIP7 tables), but projects may also be created by users for other reasons.

New variable conversions can also be accommodated for inline conversions by following `these instructions <https://gitlab.science.gc.ca/CanESM/CanESM5/wikis/how-to-use-inline-netcdf-conversion#converting-non-cmip6-variables>`_ (here "inline" refers to :code:`pycmor` running automatically as part of the CanESM diagnostics).
This method allows users to quickly add new variables to a specified project's CMOR tables, such as for model development and testing purposes.
Adding a new project is less quick, but may be more appropriate if a substantial number of new variables or changes to existing ones are required.

    .. note:: 
        For production of data published as part of official projects (such as CMIP), a new project in :code:`pycmor` should be created.

This documentation page first explains how project-specific conversions work in :code:`pycmor`. 
It then gives a step-by-step guide to setting up conversions for a new project.


How it works
^^^^^^^^^^^^^

The :code:`project` parameter identifies a set of **CMOR tables**, which are :code:`json` files giving output variable specifications for climate model output variables.
For example, CMIP6 specifies monthly atmospheric surface temperature as the variable :code:`tas` in the :code:`Amon` table with the following entry in the :code:`CMIP6_Amon.json` file:

    .. code-block:: bash

            "tas": {
                "frequency": "mon", 
                "modeling_realm": "atmos", 
                "standard_name": "air_temperature", 
                "units": "K", 
                "cell_methods": "area: time: mean", 
                "cell_measures": "area: areacella", 
                "long_name": "Near-Surface Air Temperature", 
                "comment": "near-surface (usually, 2 meter) air temperature", 
                "dimensions": "longitude latitude time height2m", 
                "out_name": "tas", 
                "type": "real", 
                "positive": "", 
                "valid_min": "", 
                "valid_max": "", 
                "ok_min_mean_abs": "", 
                "ok_max_mean_abs": ""
            }, 


This and other :code:`CMIP6_*.json` files specify formatting and metadata attributes for all output variables requested by the project.

    .. note::

        The allowed attribute names and values are part of a Controlled Vocabulary (CV) that is also specified by the project's CMOR tables.
        Information in the CMOR tables is ingested by the `CMOR community software <https://github.com/PCMDI/cmor>`_, instructing CMOR how to reformat data to produce output netcdf files complying with the formatting conventions specified by the project.
        CMOR is an acronym for Climate Model Output Rewriter, intentionally coined to sound like "see more" to indicate its purpose of enabling analysis of data from many different climate models by a wide range of users. 
        :code:`pycmor` is essentially a front-end for CMOR that interfaces between CanESM output data and the CMOR software.


All the CMOR tables and other information required by :code:`pycmor` to do netcdf conversion for a particular project is stored a folder in the :code:`ncconv/projects` directory.
To do conversions using this project's CMOR tables, set the :code:`project` parameter to the folder name (e.g., :code:`project='CMIP6'`).
A project folder has the following structure:

    .. code-block:: bash

        projects/
            CMIP6/
                cmor-tables/
                    CMIP6_Amon.json
                    ...
                cccma-tables/
                    CanESM5/
                        CMIP6-CCCma_Amon.json
                        ...
                cccma_table_defaults_CMIP6.json
                cccma_user_input_CMIP6.json
                CMIP6_source_id.json

The above example is for CMIP6, but the structure would look the same for another project (with the :code:`CMIP6` in the above directory and file names replaced with the other project's name).
Besides the aforementioned CMOR tables, which are stored in the project's :code:`cmor-tables` folder, this project folder contains several other things required for data conversion:

* The :code:`cccma-tables` folder contain :code:`json` files that map the requested output variables (which are specified by the CMOR tables) to the output variables produced by the model. These files are referred to as **CCCma tables** to avoid confusing them with the CMOR tables. Since model output depends on the model version, CCCma tables are in subfolders corresponding the model's official name (in the CMIP6 CV, the model's official name is its :code:`source_id` metadata parameter).

* :code:`cccma_user_input_CMIP6.json` specifies metadata parameters shared by all output netcdf files (e.g., institute name, contact email address, and legal license governing data use).

* :code:`cccma_table_defaults_CMIP6.json` specifies default values for CCCma table parameters. Variable-specific parameters only need to be included in CCCma tables if they differ from these default settings.

* :code:`CMIP6_source_id.json` contains information about the model (e.g., :code:`CanESM5`) that will be included in output netcdf files.

To create a new project, the above directory structure and :code:`json` files must be created.
The next section describes how to do this.

    .. note:: 
        Older versions of :code:`pycmor` assumed CMIP6 output for all conversions.
        Previous behaviour for CMIP6 conversions is obtained by setting :code:`project='CMIP6'`.


How to add a new project
^^^^^^^^^^^^^^^^^^^^^^^^

The following steps show how users can set up netcdf conversion for new projects.
As an example, assume the new project is called :code:`NewMIP` and the model producing the output to be converted is called :code:`CanESM5` (in the CMIP6 CV, the :code:`source_id` parameter gives the model name).
Assuming that the new project should be preserved in the :code:`ncconv` repo, a **new branch should be checked out** before beginning this work, and a merge request made when it's done.


\1. Create the project directory: :code:`ncconv/projects/NewMIP`


\2. Create the **CMOR tables** directory :code:`ncconv/projects/NewMIP/cmor-tables` and populate it with CMOR table :code:`json` files. 
If the project is an "official" one then presumably these tables are available somewhere and simply need to be copied into the :code:`cmor-tables` folder. 
For example, if one had to initially create the CMIP6 CMOR tables (or update them):

    .. code-block:: bash

        cd projects/CMIP6/cmor-tables
        git clone https://github.com/PCMDI/cmip6-cmor-tables
        mv cmip6-cmor-tables/Tables/*.json .
        rm -rf cmip6-cmor-tables

Alternately, CMOR tables for a new project could be created by the user. To do this, it is suggested to use existing CMOR tables from another project as templates.

    .. warning::

        :code:`pycmor` *does not* automatically carry out the data processing that is needed for output to conform to the specifications in the CMOR tables! For example, in the CMIP6 :code:`tas_Amon` example above, the CMOR table specifies :code:`"units"':' "K"`. This tells CMOR to write :code:`tas:units = "K" ;` in the output netcdf file, and it may also check that the data are within the expected numerical range for these units. But the units of the data will not *actually* be K unless they are K in the input data. The same consideration applies to any data processing choice (e.g., correct selection of pressure levels). Ensuring the correct input data is the role of the CCCma tables, explained next.


\3. Create the **CCCma tables** directory with a subfolder for the model version (CanESM5 in this example) :code:`ncconv/projects/NewMIP/cccma-tables/CanESM5` and populate it with CCCma table :code:`json` files. 
Initial versions of these tables can be created with the script :code:`ncconv/tools/create_cccma_tables.py`, which will initialize the CCCma tables with the variables listed in the CMOR tables.
The CCCma tables must be edited to ensure the correct model output variables are mapped to the variables defined in the CMOR tables. 
An additional step required for creating CCCma tables is to create the file :code:`cccma_table_defaults_NewMIP.json` in the project directory (:code:`ncconv/projects/NewMIP/`), which for convenience could be `based on the same file from another project <project_conversions.html#borrowing-from-existing-projects>`_.
This file specifies default parameter values to use for each variable in the CCCma tables (values set in the CCCma tables for individual variables will override these defaults).
See below for `further guidance on creating CCCma tables <project_conversions.html#creating-cccma-tables>`_.

    .. note::

        CCCma tables are specific to *both* the project and model version, because they map the project's requested variables (defined in the CMOR tables) to the model's output variables. 


\4. Create the "user table" :code:`json` file, :code:`cccma_user_input_NewMIP.json`.
An official project might provide an template for this file. 
Alternately, the user table from another project could be copied here, renamed, and edited.
(Note that in general the user table contains important project-specific information, and so **must** be edited if a previous project's user table is used as a template for the new project.)

\5. Add the :code:`NewMIP_source_id.json` file. For an official project, this file may be available somewhere. (For CMIP6, it's available `here <https://github.com/WCRP-CMIP/CMIP6_CVs>`_). 
This file is used to get some text describing the model version that will be included in the output netcdf files. 
For convenience it could be `borrowed from another project <project_conversions.html#borrowing-from-existing-projects>`_ provided that the other project's file has the correct model information.


Borrowing from existing projects
""""""""""""""""""""""""""""""""

Borrowing some of the input files from a previous project can simplify the creation of a new project.
They can be copied or linked into the new project directory.
For example:

    .. code-block:: bash

        cd projects/NewMIP
        ln -s ../CMIP6/cccma_table_defaults_CMIP6.json cccma_table_defaults_NewMIP.json
        ln -s ../CMIP6/CMIP6_source_id.json NewMIP_source_id.json

However, linking information to another project creates an implicit dependence on that project and future changes to it can affect the new project.
The user should decide if this is an advantage or liability.
To make the project completely independent of other projects, copy files instead of linking them.


Creating CCCma tables
"""""""""""""""""""""

Creating the CCCma tables is probably the most time-consuming step of creating a new project.
A helper utility, :code:`create_cccma_tables.py` in the :code:`ncconv/tools/` directory, is provided to simplify this process.
As an example, suppose the new project :code:`NewMIP` contains a CMOR table called :code:`Amon`, for which we want to initialize a new CCCma table to convert data from the model :code:`CanESM5`.
First ensure that the file :code:`cccma_table_defaults_NewMIP.json` already exists in the project directory, and then:

.. code-block:: bash
    
    cd projects/NewMIP
    ../../tools/create_cccma_tables.py -p NewMIP -t Amon -s CanESM5

This creates the CCCma table :code:`NewMIP-CCCma_Amon.json` in the :code:`projects/NewMIP/cccma-tables/CanESM5` directory.
This file contains a blank template entry for each variable in the CMOR table, such as:

.. code-block:: bash

    "ccb": {
        "CCCma TS var name": "",
        "CCCma diag file": ""
    },

Note that the :code:`-h` option provides more information about the command-line options.
A white-space separated list of tables can be passed (e.g., :code:`-t Amon day Omon`), and if the :code:`-t` argument is omitted then all tables in the project will be used.

.. warning::
    A variable will *not be converted* unless :code:`CCCma TS var name` and :code:`CCCma diag file` are specified.
    These parameters identify a CCCma model output variable (time series file) corresponding to the requested CMOR variable.
    If additional post-processing is needed to satisfy the CMOR variable attributes (e.g., converting units) then an :doc:`optional deck <optional_decks>` should be specified in the variable's CCCma table entry by the :code:`CCCma optional deck` parameter.

The above template provides a starting point, but still requires the user to fill in *all* of the information specifying which CCCma output variables correspond to which CMOR variables, for every variable in the table!
Often a project will use the same CMOR variable names as previous projects and the corresponding CCCma variable may also be the same as for the previous project.
For this reason, an option is provide to import the information from a previous project into the CCCma tables. 
We can modify the above example to:

.. code-block:: bash

    ../../tools/create_cccma_tables.py -p NewMIP -t Amon -s CanESM5 -pct CMIP6
    
where :code:`-pct` ("previous CCCma tables") identifies the previous project from which to import information.
The first entry in the created :code:`NewMIP-CCCma_Amon.json` file now looks like:

.. code-block:: bash

    "ccb": {
        "CCCma TS var name": "TBCD TCDCB",
        "CCCma diag file": "gp",
        "CCCma optional deck": "maskcdcb",
        "CCCma hist file": "gs",
        "CCCma hist var names": "BCD",
        "CCCma hist var frequency": "ISRAD",
        "CCCma check": "Done.",
        "CCCma comment": "%df xtraconv, maskout TCDCB<=0",
        "MIPs (requesting)": "  AerChemMIP(1,1,1,1,1), CFMIP(1,1,1,1,1), CMIP(1,1,1,1,1), DAMIP(1,1,1,1,1), FAFMIP(1,1,1), GeoMIP(1,1,1), GMMIP(1), HighResMIP(1,1,1), LUMIP(1), RFMIP(1,1,1,1,1), VolMIP(1,1)",
        "experiments": "  abrupt-4xCO2 (AerChemMIP:1,CFMIP:1,CMIP:1,DAMIP:1,FAFMIP:1,GeoMIP:1,RFMIP:1), amip (AerChemMIP:1,CFMIP:1,CMIP:1,DAMIP:1,GMMIP:1,HighResMIP:1,RFMIP:1), 1pctCO2 (AerChemMIP:1,CFMIP:1,CMIP:1,DAMIP:1,FAFMIP:1,RFMIP:1), historical (AerChemMIP:1,CFMIP:1,CMIP:1,DAMIP:1,GeoMIP:1,HighResMIP:1,LUMIP:1,RFMIP:1,VolMIP:1), piControl (AerChemMIP:1,CFMIP:1,CMIP:1,DAMIP:1,FAFMIP:1,GeoMIP:1,HighResMIP:1,RFMIP:1,VolMIP:1)"
    },

where the essential parameters as well as a bunch of non-essential (but hopefully informative) parameters from the other project's CCCma table have been imported.
The import relies on matching the CMOR variable name and table name (here :code:`ccb` and :code:`Amon`, respectively) between the previous and new projects.
Of course this import does not guarantee that the previous project's CCCma table information will be correct for the new project, but it might provide a more useful starting point than blank templates.

Beyond the command-line arguments, more fine-grained control of :code:`create_cccma_tables.py` is possible with a user input file.
The template users can copy and modify is :code:`create_cccma_tables.json` in :code:`ncconv/tools/`.
If a file with this name is placed in the current directory then :code:`create_cccma_tables.py` will automatically use it (alternately the :code:`-ui` option can be used to specify the user input file).
Repeating the above example:

.. code-block:: bash

    ../../tools/create_cccma_tables.py -p NewMIP -t Amon -s CanESM5 -pct CMIP6

but now with a file :code:`create_cccma_tables.json` in the current directory that contains:

.. code-block:: bash

    "cmor_vars" : {
        "Amon" : ["pr", "tas"],
    },
    "pct_exclude_parameters" : ["MIPs (requesting)", "experiments"]

will result in only the variables :code:`pr` and :code:`tas` being placed in the :code:`NewMIP-CCCma_Amon.json` file, and the parameters :code:`MIPs (requesting)` and :code:`experiments` will be excluded from the import.
If table names differ between the previous project and the new one, the :code:`pct_tables` option will map from new table names to previous ones. 
For example, if the new project has table :code:`mon` for monthly data, and we want to import information into it from previous table :code:`Amon`, then in the user input file set:

.. code-block:: bash

    "pct_tables" : {
        "mon" : "Amon"
    },

See the example user input file :code:`ncconv/tools/create_cccma_tables.json` for other available input arguments.
