
The Conversion Process
----------------------

In order to execute a conversion, :code:`pycmor` gets information from 
a lot of different locations, and runs it all through a complex pipeline
in order to produce "CMORized" netcdf output files. The following flow chart
has been provided to aid users in understanding how it works. 
:doc:`Project-specific <project_conversions>` input information is indicated with orange text.


.. figure:: imgs/pycmor_convflow_v2.png
    :align: center

..
    source file for the diagram:
    https://drive.google.com/file/d/1e6it2FOoTlRxnu7v3cMVLxpiFf0qGnSI/view?usp=sharing
    v2 is an update for project-specific conversions, starting from Clint's original version:
    https://drive.google.com/file/d/1dg_-DM1bDqPnu3NqhEQkX6OsWpTFueto/view?usp=sharing
    export as png to show in sphinx
    (draw.io can export a vector pdf, but seems that sphinx won't display it)



